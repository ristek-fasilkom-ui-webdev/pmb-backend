from akun.models import Profil, Maba
from django.shortcuts import render, get_object_or_404
from django.conf import settings
from rest_framework import generics, mixins, permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from situs.models import Announcement, Event, Task, Submission, QnA, Feedback, Story, Notification, KataElemen
import situs.serializers as serializers
import datetime
import requests


class FileUpload(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = serializers.FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            serializer_data = file_serializer.data
            serializer_data['file'] = settings.HOST_DOMAIN_BASE_URL + serializer_data['file']
            return Response(serializer_data, status=status.HTTP_201_CREATED)
        return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnnouncementList(generics.ListAPIView):
    queryset = Announcement.objects.all()
    serializer_class = serializers.AnnouncementSerializer
    permission_classes = (permissions.IsAuthenticated,)


class AnnouncementDetail(generics.RetrieveAPIView):
    queryset = Announcement.objects.all()
    serializer_class = serializers.AnnouncementDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)


class EventList(generics.ListAPIView):
    queryset = Event.objects.all()
    serializer_class = serializers.EventSerializer
    permission_classes = (permissions.IsAuthenticated,)


class EventDetail(generics.RetrieveAPIView):
    queryset = Event.objects.all()
    serializer_class = serializers.EventDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)


class TaskList(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        user = request.user
        submissions = Submission.objects.filter(user=user)
        submissions_task_ids = { s.task.id for s in submissions }
        
        tasks = Task.objects.all()
        serialized_tasks = serializers.ReadOnlyTaskSerializer(tasks, many=True, context={'submissions_task_ids': submissions_task_ids})
        
        return Response(serialized_tasks.data, status=status.HTTP_200_OK)

class TaskDetail(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk):
        user = request.user

        task = get_object_or_404(Task, id=pk)
        serialized_task = serializers.ReadOnlyTaskDetailSerializer(task, context={'is_submission_exist': Submission.objects.filter(user=user, task=task).exists()})
        
        return Response(serialized_task.data, status=status.HTTP_200_OK)


class TaskSubmissionList(APIView):
    def get_task(self, pk_task):
        return get_object_or_404(Task, id=pk_task)

    def get(self, request, pk_task):
        task = self.get_task(pk_task)
        submissions = Submission.objects.filter(task=task)

        if request.GET.get('user') == 'me':
            submissions = submissions.filter(user=request.user)

        serialized_submissions = serializers.SubmissionSerializer(submissions, many=True)
        return Response(serialized_submissions.data)

    def post(self, request, pk_task):
        task = self.get_task(pk_task)
        
        data = request.data
        data['user'] = self.request.user.id
        data['task'] = task.id

        serialized_submission = serializers.CreateSubmissionSerializer(data=data)
        if serialized_submission.is_valid():
            created_submission = serialized_submission.save()
            serialized_created_submission = serializers.SubmissionSerializer(created_submission)
            return Response(serialized_created_submission.data, status=status.HTTP_201_CREATED)
        return Response(serialized_submission.errors, status=status.HTTP_400_BAD_REQUEST)


class QnACreate(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        data = request.data
        data['user'] = self.request.user.id

        serialized_qna = serializers.QnASerializer(data=data)
        if serialized_qna.is_valid():
            created_qna = serialized_qna.save()
            serialized_created_qna = serializers.ReadOnlyQnASerializer(created_qna)
            return Response(serialized_created_qna.data, status=status.HTTP_201_CREATED)
        return Response(serialized_qna.errors, status=status.HTTP_400_BAD_REQUEST)


class FeedbackList(generics.ListAPIView):
    serializer_class = serializers.FeedbackSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        profil = get_object_or_404(Profil, user=self.request.user)
        maba = get_object_or_404(Maba, profil=profil)

        return Feedback.objects.filter(target=maba)


class StoryList(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        stories = Story.objects.all()
        if request.GET.get('user') == 'me':
            stories = stories.filter(user=request.user)
        
        serialized_stories = serializers.GetStorySerializer(stories, many=True)
        return Response(serialized_stories.data, status=status.HTTP_200_OK)

    def post(self, request):
        data = request.data
        data['user'] = request.user.id
        check_url = requests.get(data['link'])
        if check_url.status_code == 200 and 'medium.com' in data['link']:
            serialized_story = serializers.StorySerializer(data=data)
            if serialized_story.is_valid():
                created_story = serialized_story.save()
                serialized_created_story = serializers.GetStorySerializer(created_story)
                return Response(serialized_created_story.data, status=status.HTTP_201_CREATED)
            return Response(serialized_story.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(data={'Detail': 'Medium url invalid'}, status=status.HTTP_406_NOT_ACCEPTABLE)


class StoryDetail(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk):
        story_obj = get_object_or_404(Story, id=pk)
        serialized_story = serializers.GetStorySerializer(story_obj)
        return Response(serialized_story.data, status=status.HTTP_200_OK)

    def patch(self, request, pk):
        story_obj = get_object_or_404(Story, id=pk)
        if story_obj.user != request.user:
            return Response(data={'Detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)
        
        data = request.data
        check_url = requests.get(data['link'])
        if check_url.status_code == 200 and 'medium.com' in data['link']:
            allowed_data = {}
            allowed_data['link'] = data['link']
            serialized_story = serializers.StorySerializer(story_obj, data=allowed_data, partial=True)
            if serialized_story.is_valid():
                modified_story = serialized_story.save()
                serialized_modified_story = serializers.GetStorySerializer(modified_story)
                return Response(serialized_modified_story.data)
            return Response(serialized_story.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(data={'Detail': 'Medium url invalid'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    
    def delete(self, request, pk):
        story_obj = get_object_or_404(Story, id=pk)
        if story_obj.user != request.user:
            return Response(data={'Detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)
        
        story_obj.delete()
        return Response(data={'Detail': 'Deleted'}, status=status.HTTP_200_OK)
        

class NotificationList(generics.ListAPIView):
    serializer_class = serializers.NotificationSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Notification.objects.filter(user=self.request.user, is_viewed=False)
    
    def patch(self, request):
        Notification.objects.filter(user=self.request.user, is_viewed=False).update(is_viewed=True)
        return Response({'Detail': 'All pending notification set as viewed'}, status=status.HTTP_200_OK)


class NotificationDetail(APIView):
    permission_classes = [permissions.IsAuthenticated]
    
    def get(self, request, pk):
        notification_obj = get_object_or_404(Notification, id=pk, user=self.request.user)
        serialized_notification = serializers.NotificationSerializer(notification_obj)
        return Response(serialized_notification.data, status=status.HTTP_200_OK)

    def patch(self, request, pk):
        try:
            notification = Notification.objects.get(id=pk, user=self.request.user)
            notification.is_viewed = True
            notification.save()
            return Response({'Detail': 'Notification set as viewed'}, status=status.HTTP_200_OK)
        except Notification.DoesNotExist:
            return Response({'Detail': 'Notification does not exist or not owned'}, status=status.HTTP_400_BAD_REQUEST)

class KataElemenList(generics.ListAPIView):
    queryset = KataElemen.objects.all()
    serializer_class = serializers.ReadOnlyKataElemenSerializer
    permission_classes = (permissions.IsAuthenticated,) 

    def post(self, request):
        data = request.data
        data['user'] = self.request.user.id

        serialized_kata_elemen = serializers.KataElemenSerializer(data=data)
        if serialized_kata_elemen.is_valid():
            serialized_kata_elemen.save()
            return super().get(self, request)
        return Response(serialized_kata_elemen.errors, status=status.HTTP_400_BAD_REQUEST)

class KataElemenDetail(APIView):
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, pk):
        kata_elemen = get_object_or_404(KataElemen, id=pk)
        serializer_kata_elemen = serializers.ReadOnlyKataElemenSerializer(kata_elemen)

        return Response(serializer_kata_elemen.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        kata_elemen_obj = get_object_or_404(KataElemen, id=pk)
        if kata_elemen_obj.user == request.user:
            kata_elemen_obj.delete()
            return Response(data={'Detail': 'Deleted'}, status=status.HTTP_200_OK)
        else :
            return Response(data={'Detail': 'You cant delete other people message'}, status=status.HTTP_403_FORBIDDEN)


@api_view(['GET'])
@permission_classes([permissions.AllowAny])
def get_server_time(request):
    now = datetime.datetime.now()
    return Response({'server_time': now})
