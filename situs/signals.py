from akun.models import Maba, Elemen
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from situs.models import Announcement, Event, Task, Feedback, Story, Notification, NotificationType

"""
Rely heavily on correct url patterns for models handled here. 
Any change on url patterns to access models here should affect this file.
"""

@receiver(post_save, sender=Announcement)
def create_notifications_when_new_announcement_created(sender, created, instance, **kwargs):
    if created:
        maba_list = Maba.objects.all()
        notification_list = [
            Notification(
                user=maba.profil.user, 
                notification_type=NotificationType.NEW_ANNOUNCEMENT.name, 
                related_object_type=ContentType.objects.get(app_label='situs', model='announcement'),
                related_object_id=instance.id,
                is_viewed=False
            ) for maba in maba_list
        ]
        Notification.objects.bulk_create(notification_list)


@receiver(post_save, sender=Event)
def create_notifications_when_new_event_created(sender, created, instance, **kwargs):
    if created:
        maba_list = Maba.objects.all()
        notification_list = [
            Notification(
                user=maba.profil.user, 
                notification_type=NotificationType.NEW_EVENT.name, 
                related_object_type=ContentType.objects.get(app_label='situs', model='event'),
                related_object_id=instance.id,
                is_viewed=False
            ) for maba in maba_list
        ]
        Notification.objects.bulk_create(notification_list)


@receiver(post_save, sender=Task)
def create_notifications_when_new_task_created(sender, created, instance, **kwargs):
    if created:
        maba_list = Maba.objects.all()
        notification_list = [
            Notification(
                user=maba.profil.user, 
                notification_type=NotificationType.NEW_TASK.name, 
                related_object_type=ContentType.objects.get(app_label='situs', model='task'),
                related_object_id=instance.id,
                is_viewed=False
            ) for maba in maba_list
        ]
        Notification.objects.bulk_create(notification_list)


@receiver(post_save, sender=Story)
def create_notifications_when_new_story_created(sender, created, instance, **kwargs):
    if created:
        user_list = User.objects.all()
        notification_list = [
            Notification(
                user=user, 
                notification_type=NotificationType.NEW_STORY.name, 
                related_object_type=ContentType.objects.get(app_label='situs', model='story'),
                related_object_id=instance.id,
                is_viewed=False
            ) for user in user_list
        ]
        Notification.objects.bulk_create(notification_list)


@receiver(post_save, sender=Feedback)
def create_notification_when_maba_get_new_feedback(sender, created, instance, **kwargs):
    if created:
        maba = instance.target
        Notification.objects.create(
            user=maba.profil.user, 
            notification_type=NotificationType.NEW_FEEDBACK.name, 
            related_object_type=ContentType.objects.get(app_label='situs', model='feedback'),
            related_object_id=instance.id,
            is_viewed=False
        )