from akun.models import Maba
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from enum import Enum


class File(models.Model):
    file = models.FileField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']
        verbose_name = 'File'
        verbose_name_plural = 'Files'

    def __str__(self):
        return self.file.name


class Announcement(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    description = RichTextField(blank=True, null=True)
    attachment_link = models.URLField(blank=True, null=True, help_text='Must start with "https://" or "http://", eg. "https://google.com"')

    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date']
        verbose_name = 'Announcement'
        verbose_name_plural = 'Announcements'

    def __str__(self):
        return self.title


class Event(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    place = models.CharField(max_length=255)
    description = RichTextField(blank=True, null=True)
    attachment_link = models.URLField(blank=True, null=True, help_text='Must start with "https://" or "http://", eg. "https://google.com"')
    cover_image_link = models.URLField(blank=True, null=True, help_text='Must start with "https://" or "http://", eg. "https://google.com"')
    organizer = models.CharField(max_length=255, blank=True, null=True)
    organizer_image_link = models.URLField(blank=True, null=True, help_text='Must start with "https://" or "http://", eg. "https://google.com"')

    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date']
        verbose_name = 'Event'
        verbose_name_plural = 'Events'

    def __str__(self):
        return self.title


class Task(models.Model):
    title = models.CharField(max_length=255)
    start_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    end_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    description = RichTextField(blank=True, null=True)
    attachment_link = models.URLField(blank=True, null=True, help_text='Must start with "https://" or "http://", eg. "https://google.com"')
    can_have_submission = models.BooleanField(default=True)

    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['end_date', 'start_date']
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'

    def __str__(self):
        return self.title


class SubmissionStatus(Enum):
    PENDING = 'Pending'
    ACCEPTED = 'Accepted'
    REJECTED = 'Rejected'

    @staticmethod
    def get_default():
        return SubmissionStatus.PENDING.name


class Submission(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    file_link = models.URLField(max_length=255, help_text='Must start with "https://" or "http://", eg. "https://google.com"')
    submit_time = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    status = models.CharField(max_length=10, choices=[(status.name, status.value) for status in SubmissionStatus], default=SubmissionStatus.get_default())

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-submit_time', '-created_at']
        verbose_name = 'Submission'
        verbose_name_plural = 'Submissions'

    def __str__(self):
        return "Submission by " + self.user.username + " for task " + self.task.title


class QnA(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    post_id = models.PositiveIntegerField()
    post_object = GenericForeignKey('post_type', 'post_id')
    comment = RichTextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at', '-updated_at']
        verbose_name = 'Question and Answer'
        verbose_name_plural = 'Questions and Answers'

    def __str__(self):
        return "Comment by " + self.user.username + " on " + self.post_type.model_class().__name__ + " " + self.post_object.title


class Feedback(models.Model):
    target = models.ForeignKey(Maba, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    content = models.TextField()
    image = models.ImageField(blank=True, null=True)
    initial = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at', '-updated_at']
        verbose_name = 'Feedback'
        verbose_name_plural = 'Feedbacks'

    def __str__(self):
        return "Feedback titled " + self.title + " for " + self.target.profil.nama_lengkap + " by " + self.initial


class Story(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    link = models.URLField(help_text='Must start with "https://" or "http://"')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at', '-updated_at']
        verbose_name = 'Story'
        verbose_name_plural = 'Stories'

    def __str__(self):
        return "Story by " + self.user.profil.nama_lengkap + " at " + self.link


class NotificationType(Enum):
    NEW_ANNOUNCEMENT = 'NEW_ANNOUNCEMENT'
    NEW_EVENT = 'NEW_EVENT'
    NEW_TASK = 'NEW_TASK'
    NEW_FEEDBACK = 'NEW_FEEDBACK'
    NEW_STORY = 'NEW_STORY'
    KENALAN_ACCEPTED = 'KENALAN_ACCEPTED'
    KENALAN_REJECTED = 'KENALAN_REJECTED'
    KENALAN_PENDING = 'KENALAN_PENDING'


class Notification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notification_type = models.CharField(max_length=255, choices=[(notification_type.name, notification_type.value) for notification_type in NotificationType])
    related_object_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    related_object_id = models.PositiveIntegerField()
    related_object = GenericForeignKey('related_object_type', 'related_object_id')
    is_viewed = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at', '-updated_at']
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'

    def __str__(self):
        return "Notification for " + self.user.profil.nama_lengkap + " with type " + self.notification_type

class KataElemen(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    kata_elemen = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at', '-updated_at']
        verbose_name = 'KataElemen'
        verbose_name_plural = 'KataElemens'

    def __str__(self):
        return "Kata elemen from " + self.user.profil.nama_lengkap
    
