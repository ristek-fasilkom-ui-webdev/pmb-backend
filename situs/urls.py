from django.urls import path, re_path, include
from rest_framework.urlpatterns import format_suffix_patterns
from situs import views

urlpatterns = [
    re_path(r'^upload-file$', views.FileUpload.as_view(), name='file_upload'),
    re_path(r'^announcements$', views.AnnouncementList.as_view(), name='announcement_list'),
    re_path(r'^announcements/(?P<pk>[0-9]+)$', views.AnnouncementDetail.as_view(), name='announcement_detail'),
    re_path(r'^events$', views.EventList.as_view(), name='list_event'),
    re_path(r'^events/(?P<pk>[0-9]+)$', views.EventDetail.as_view(), name='detail_event'),
    re_path(r'^kata-elemens$', views.KataElemenList.as_view(), name='list_kata_elemen'),
    re_path(r'^kata-elemens/(?P<pk>[0-9]+)$', views.KataElemenDetail.as_view(), name='detail_kata_elemen'),
    re_path(r'^tasks$', views.TaskList.as_view(), name='list_task'),
    re_path(r'^tasks/(?P<pk>[0-9]+)$', views.TaskDetail.as_view(), name='detail_task'),
    re_path(r'^tasks/(?P<pk_task>[0-9]+)/submissions$', views.TaskSubmissionList.as_view(), name='task_submission_list'),
    re_path(r'^qnas$', views.QnACreate.as_view(), name='qnas_create'),
    re_path(r'^feedbacks$', views.FeedbackList.as_view(), name='feedback_list'),
    re_path(r'^notifications$', views.NotificationList.as_view(), name='notification_list'),
    re_path(r'^notifications/(?P<pk>[0-9]+)$', views.NotificationDetail.as_view(), name='detail_notification'),
    re_path(r'^stories$', views.StoryList.as_view(), name='story_list'),
    re_path(r'^stories/(?P<pk>[0-9]+)$', views.StoryDetail.as_view(), name='detail_story'),
    re_path(r'server-time', views.get_server_time, name="server_time"),
]
