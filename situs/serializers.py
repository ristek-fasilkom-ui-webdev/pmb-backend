from akun.serializers import UserSerializer, UserDetailSerializer
from django.contrib.contenttypes.models import ContentType
from generic_relations.relations import GenericRelatedField
from rest_framework import serializers
from situs.models import File, Announcement, Event, Task, Submission, QnA, Feedback, Story, Notification, KataElemen
from teman.models import Kenalan
from teman.serializers import GetKenalanSerializer
from akun.serializers import AngkatanSerializer, ElemenSerializer
import json
import requests

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = '__all__'
        read_only_fields = ['created_at']

class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Announcement
        fields = '__all__'


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'

class ReadOnlyTaskSerializer(serializers.ModelSerializer):
    is_submitted = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = '__all__'

    def get_is_submitted(self, obj):
        submissions_task_ids = self.context.get('submissions_task_ids')

        if submissions_task_ids is None:
            return False
        
        if obj.can_have_submission and obj.id in submissions_task_ids:
            return True
        
        return False


class SubmissionSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    task = TaskSerializer()

    class Meta:
        model = Submission
        fields = '__all__'
        read_only_fields = ['created_at', 'updated_at']


class CreateSubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ['user', 'task', 'file_link', 'submit_time']


class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = '__all__'


class QnASerializer(serializers.ModelSerializer):
    post_type = serializers.SlugRelatedField(
        slug_field='model',
        queryset=ContentType.objects.all()
    )

    class Meta:
        model = QnA
        fields = ['user', 'post_type', 'post_id', 'comment']
    
    def validate_post_type(self, value):
        """
        Check that the post type is Announcement, Task, or Event.
        """
        model = value.model

        if 'announcement' not in model and 'task' not in model and 'event' not in model:
            raise serializers.ValidationError("Invalid post type")
        return value


class ReadOnlyQnASerializer(serializers.ModelSerializer):
    user = UserSerializer()
    post_type = ContentTypeSerializer()
    post_object = GenericRelatedField({
        Announcement: AnnouncementSerializer(),
        Event: EventSerializer(),
        Task: TaskSerializer()
    })

    class Meta:
        model = QnA
        fields = ['user', 'post_type', 'post_object', 'comment', 'created_at', 'updated_at']


class QnAEmbeddedSerializer(serializers.ModelSerializer):
    user = UserDetailSerializer()

    class Meta:
        model = QnA
        fields = ['user', 'comment', 'created_at', 'updated_at']


class AnnouncementDetailSerializer(AnnouncementSerializer):
    qnas = serializers.SerializerMethodField()

    def get_qnas(self, obj):
        qnas_objs = QnA.objects.filter(post_type__model='announcement', post_id=obj.id)
        serialized_qnas = QnAEmbeddedSerializer(qnas_objs, many=True)

        return serialized_qnas.data

class EventDetailSerializer(EventSerializer):
    qnas = serializers.SerializerMethodField()

    def get_qnas(self, obj):
        qnas_objs = QnA.objects.filter(post_type__model='event', post_id=obj.id)
        serialized_qnas = QnAEmbeddedSerializer(qnas_objs, many=True)

        return serialized_qnas.data


class ReadOnlyTaskDetailSerializer(ReadOnlyTaskSerializer):
    qnas = serializers.SerializerMethodField()

    def get_qnas(self, obj):
        qnas_objs = QnA.objects.filter(post_type__model='task', post_id=obj.id)
        serialized_qnas = QnAEmbeddedSerializer(qnas_objs, many=True)

        return serialized_qnas.data
    
    def get_is_submitted(self, obj):
        return self.context.get('is_submission_exist')


class FeedbackSerializer(serializers.ModelSerializer):
    image_link = serializers.ImageField(source='image')

    class Meta:
        model = Feedback
        fields = ['title', 'content', 'image_link', 'initial', 'created_at', 'updated_at']


class StorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        fields = '__all__'
    
    def validate_link(self, value):
        medium_response = requests.get(value + '?format=json')
        if medium_response.status_code != 200:
            raise serializers.ValidationError

        try:
            medium_json = json.loads(medium_response.text[16:])['payload']['value']
            
            try:
                title = medium_json['title']
            except:
                raise serializers.ValidationError
            
            try:
                latest_published_at = medium_json['latestPublishedAt']
            except:
                raise serializers.ValidationError

            try:
                subtitle = medium_json['content']['subtitle']
            except:
                raise serializers.ValidationError
        except:
            raise serializers.ValidationError
        return value


class GetStorySerializer(serializers.ModelSerializer):
    user = UserDetailSerializer()
    medium = serializers.SerializerMethodField()

    class Meta:
        model = Story
        fields = '__all__'

    def get_medium(self, obj):
        medium_response = requests.get(obj.link + '?format=json')
        if medium_response.status_code != 200:
            raise serializers.ValidationError

        try:
            medium_json = json.loads(medium_response.text[16:])['payload']['value']
            
            try:
                title = medium_json['title']
            except:
                raise serializers.ValidationError
            
            try:
                latest_published_at = medium_json['latestPublishedAt']
            except:
                raise serializers.ValidationError

            try:
                subtitle = medium_json['content']['subtitle']
            except:
                raise serializers.ValidationError

            thumbnail = None
            for paragraph in medium_json['content']['bodyModel']['paragraphs']:
                if paragraph['type'] == 4:
                    thumbnail = paragraph
                    break
            return {'title': title,'latest_published_at': latest_published_at,'subtitle': subtitle,'thumbnail': thumbnail}
        except:
            return {'title': None,'latest_published_at': None,'subtitle': None,'thumbnail': None}

class NotificationSerializer(serializers.ModelSerializer):
    related_object_type = ContentTypeSerializer()
    related_object = GenericRelatedField({
        Announcement: AnnouncementSerializer(),
        Event: EventSerializer(),
        Task: TaskSerializer(),
        Feedback: FeedbackSerializer(),
        Story: GetStorySerializer(),
        Kenalan: GetKenalanSerializer(),
    })

    class Meta:
        model = Notification
        fields = '__all__'

class KataElemenSerializer(serializers.ModelSerializer):
    class Meta:
        model = KataElemen
        fields = '__all__'


class ReadOnlyKataElemenSerializer(serializers.ModelSerializer):
    user = ElemenSerializer()

    class Meta:
        model = KataElemen
        fields = '__all__'
