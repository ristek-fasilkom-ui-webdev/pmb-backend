from django.apps import AppConfig


class SitusConfig(AppConfig):
    name = 'situs'

    def ready(self):
        import situs.signals
