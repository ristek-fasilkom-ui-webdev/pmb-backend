from django.contrib import admin
from django.utils.html import format_html
from situs.models import File, Announcement, Event, Task, Submission, QnA, Feedback, Story, Notification, KataElemen


class AnnouncementModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'date']
    search_fields = ['title']

    class Meta:
        model = Announcement


class EventModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'date', 'place', 'organizer']
    search_fields = ['title', 'place', 'organizer']
    list_filter = ['organizer']

    class Meta:
        model = Event


class TaskModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_date', 'end_date', 'can_have_submission']
    search_fields = ['title']
    list_filter = ['can_have_submission']

    class Meta:
        model = Task


class SubmissionModelAdmin(admin.ModelAdmin):
    list_display = ['profil', 'task', 'file_link_clickable', 'submit_time', 'created_at', 'pengoreksi']
    list_display_links = ['profil', 'task']
    search_fields = ['user__profil__nama_lengkap', 'task__title']
    list_filter = ['task', 'user__profil__maba__pengoreksi']
    readonly_fields = ['created_at']

    def profil(self, obj):
        return obj.user.profil
    
    def pengoreksi(self, obj):
        return obj.user.profil.maba.pengoreksi
    
    def file_link_clickable(self, obj):
        return format_html('<a href="{}">{}</a>', obj.file_link, obj.file_link)
    
    profil.admin_order_field = 'user__profil__nama_lengkap'
    pengoreksi.admin_order_field = 'user__profil__maba__pengoreksi'
    file_link_clickable.short_description = 'File Link'

    class Meta:
        model = Submission


class FeedbackModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'target', 'initial']
    search_fields = ['title', 'target__profil__nama_lengkap', 'initial']
    list_filter = ['title', 'initial']

    class Meta:
        model = Feedback


admin.site.register(File)
admin.site.register(Announcement, AnnouncementModelAdmin)
admin.site.register(Event, EventModelAdmin)
admin.site.register(Task, TaskModelAdmin)
admin.site.register(Submission, SubmissionModelAdmin)
admin.site.register(QnA)
admin.site.register(Feedback, FeedbackModelAdmin)
admin.site.register(Story)
admin.site.register(Notification)
admin.site.register(KataElemen)
