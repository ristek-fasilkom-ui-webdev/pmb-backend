from akun.constants import NAMA_ANGKATAN, NAMA_ANGKATAN_NON_AKTIF
from akun.models import Maba
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from teman.models import Kenalan, KenalanStatus, KenalanUserStatistics

class Command(BaseCommand):
    help = 'Manually update all kenalan statistics'

    def handle(self, *args, **options):
        maba_list = Maba.objects.all()
        for maba in maba_list:
            user_maba = maba.profil.user
            user_statistics = KenalanUserStatistics.objects.filter(user=user_maba)
            for user_statistic in user_statistics:
                kenalan_task = user_statistic.kenalan_task
                now = timezone.now()
                if kenalan_task.end_date >= now and kenalan_task.start_date <= now:
                    """
                    Angkatan specific fields. Change as necessary
                    """
                    user_statistic.kenalan_created_quanta = Kenalan.objects.filter(
                        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2018']).exclude(status=KenalanStatus.NOT_FRIEND.name).count()
                    user_statistic.kenalan_created_tarung = Kenalan.objects.filter(
                        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2017']).exclude(status=KenalanStatus.NOT_FRIEND.name).count()
                    user_statistic.kenalan_created_omega = Kenalan.objects.filter(
                        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2016']).exclude(status=KenalanStatus.NOT_FRIEND.name).count()
                    user_statistic.kenalan_created_elemen = Kenalan.objects.filter(
                        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN_NON_AKTIF).count()
                    user_statistic.kenalan_created_all = Kenalan.objects.filter(
                        user_maba=user_maba).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

                    user_statistic.kenalan_approved_quanta = Kenalan.objects.filter(
                        user_maba=user_maba,
                        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2018'],
                        status=KenalanStatus.ACCEPTED.name
                    ).count()

                    user_statistic.kenalan_approved_tarung = Kenalan.objects.filter(
                        user_maba=user_maba,
                        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2017'],
                        status=KenalanStatus.ACCEPTED.name
                    ).count()

                    user_statistic.kenalan_approved_omega = Kenalan.objects.filter(
                        user_maba=user_maba,
                        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2016'],
                        status=KenalanStatus.ACCEPTED.name
                    ).count()

                    user_statistic.kenalan_approved_elemen = Kenalan.objects.filter(
                        user_maba=user_maba,
                        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN_NON_AKTIF,
                        status=KenalanStatus.ACCEPTED.name
                    ).count()

                    user_statistic.kenalan_approved_all = Kenalan.objects.filter(
                        user_maba=user_maba,
                        status=KenalanStatus.ACCEPTED.name
                    ).count()

                    user_statistic.save()
            