from django.urls import path, re_path
from teman import views

urlpatterns = [
    re_path(r'^token$', views.TokenView.as_view(), name='get_token'),
    re_path(r'^kenalan$', views.KenalanList.as_view(), name='list_kenalan'),
    re_path(r'^kenalan/(?P<pk>[0-9]+)$', views.KenalanDetail.as_view(), name='detail_kenalan'),
    re_path(r'^kenalan-task$', views.KenalanTaskList.as_view(), name='list_kenalan_task'),
    re_path(r'^kenalan-user-statistics$', views.KenalanUserStatisticsList.as_view(), name='list_kenalan_user_statistic'),
    re_path(r'^find-friends$', views.FindFriend.as_view(), name='find_friend'),
]
