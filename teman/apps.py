from django.apps import AppConfig


class TemanConfig(AppConfig):
    name = 'teman'

    def ready(self):
        import teman.signals
