from django.contrib import admin
from hephaestus.admin import ExportCsvMixin
from teman.models import Token, Kenalan, KenalanTask, KenalanUserStatistics, KenalanStatus


class KenalanModelAdmin(admin.ModelAdmin):
    list_display = ['profil_maba', 'profil_elemen', 'description', 'status']
    search_fields = ['user_maba__profil__nama_lengkap', 'user_elemen__profil__nama_lengkap']
    list_filter = ['status']

    def profil_maba(self, obj):
        return obj.user_maba.profil

    def profil_elemen(self, obj):
        return obj.user_elemen.profil

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Kenalan.objects.all()
        return Kenalan.objects.all().exclude(status=KenalanStatus.NOT_FRIEND.name)
    
    profil_maba.admin_order_field = 'user_maba__profil__nama_lengkap'
    profil_elemen.admin_order_field = 'user_elemen__profil__nama_lengkap'

    class Meta:
        model = Kenalan


class KenalanTaskModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_date', 'end_date', 'required_quanta', 'required_tarung', 'required_omega', 'required_elemen', 'required_all']
    search_fields = ['title']

    class Meta:
        model = KenalanTask


class KenalanUserStatisticModelAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ['user_nama_lengkap', 'kenalan_task_title', 'kenalan_created_quanta', 'kenalan_created_tarung', 'kenalan_created_omega', 'kenalan_created_elemen',\
                    'kenalan_created_all', 'kenalan_approved_quanta', 'kenalan_approved_tarung', 'kenalan_approved_omega', 'kenalan_approved_elemen', 'kenalan_approved_all']
    search_fields = ['user__profil__nama_lengkap', 'kenalan_task__title']
    list_filter = ['kenalan_task__title']
    actions = ["export_as_csv"]

    def user_nama_lengkap(self, obj):
        return obj.user.profil.nama_lengkap

    def kenalan_task_title(self, obj):
        return obj.kenalan_task.title
    
    user_nama_lengkap.admin_order_field = 'user__profil__nama_lengkap'
    kenalan_task_title.admin_order_field = 'kenalan_task__title'

    class Meta:
        model = KenalanUserStatistics


admin.site.register(Token)
admin.site.register(Kenalan, KenalanModelAdmin)
admin.site.register(KenalanTask, KenalanTaskModelAdmin)
admin.site.register(KenalanUserStatistics, KenalanUserStatisticModelAdmin)
