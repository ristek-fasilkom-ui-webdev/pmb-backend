from akun.constants import NAMA_ANGKATAN, NAMA_ANGKATAN_NON_AKTIF
from django.utils import timezone
from teman.models import Token, Kenalan, KenalanStatus

def delete_expired_and_used_token(user):
    delete_expired_token(user)
    delete_used_token(user)

def delete_expired_token(user):
    now = timezone.now()

    expired_token = Token.objects.filter(user=user, end_time__lt=now)
    if expired_token.exists():
        expired_token.delete()


def delete_used_token(user):
    used_token = Token.objects.filter(user=user, count__lte=0)
    if used_token.exists():
        used_token.delete()


def update_kenalan_statistic_for_user_maba(user_statistic, user_maba):
    """
    Angkatan specific fields. Change as necessary
    """
    user_statistic.kenalan_created_quanta = Kenalan.objects.filter(
        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2018']).exclude(status=KenalanStatus.NOT_FRIEND.name).count()
    user_statistic.kenalan_created_tarung = Kenalan.objects.filter(
        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2017']).exclude(status=KenalanStatus.NOT_FRIEND.name).count()
    user_statistic.kenalan_created_omega = Kenalan.objects.filter(
        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2016']).exclude(status=KenalanStatus.NOT_FRIEND.name).count()
    user_statistic.kenalan_created_elemen = Kenalan.objects.filter(
        user_maba=user_maba, user_elemen__profil__angkatan__nama=NAMA_ANGKATAN_NON_AKTIF).count()
    user_statistic.kenalan_created_all = Kenalan.objects.filter(
        user_maba=user_maba).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

    user_statistic.kenalan_approved_quanta = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2018'],
        status=KenalanStatus.ACCEPTED.name
    ).count()

    user_statistic.kenalan_approved_tarung = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2017'],
        status=KenalanStatus.ACCEPTED.name
    ).count()

    user_statistic.kenalan_approved_omega = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN['2016'],
        status=KenalanStatus.ACCEPTED.name
    ).count()

    user_statistic.kenalan_approved_elemen = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profil__angkatan__nama=NAMA_ANGKATAN_NON_AKTIF,
        status=KenalanStatus.ACCEPTED.name
    ).count()

    user_statistic.kenalan_approved_all = Kenalan.objects.filter(
        user_maba=user_maba,
        status=KenalanStatus.ACCEPTED.name
    ).count()

    user_statistic.save()