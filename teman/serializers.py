from akun.serializers import UserSerializer, MabaSerializer, ElemenSerializer
from akun.models import Maba, Elemen
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from teman.models import Token, Kenalan, KenalanStatus, KenalanTask, KenalanUserStatistics

class TokenSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Token
        fields = '__all__'


class MabaKenalanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kenalan
        fields = ['description', 'status']
    
    def update(self, instance, validated_data):
        if instance.status == KenalanStatus.ACCEPTED.name:
            raise serializers.ValidationError('You cannot edit kenalan that is already accepted!')
        elif instance.status == KenalanStatus.PENDING.name:
            raise serializers.ValidationError('You cannot edit kenalan that is already submitted!')
        
        if validated_data['status'] == KenalanStatus.SAVED.name:
            if Kenalan.objects.filter(user_maba=instance.user_maba, status=KenalanStatus.SAVED.name).count() >= 10:
                raise serializers.ValidationError('You can only save up to 10 kenalan!')    
        elif validated_data['status'] == KenalanStatus.NOT_FRIEND.name:
            raise serializers.ValidationError('You cannot change kenalan status to Not Friend!')

        instance.description = validated_data['description']
        instance.status = validated_data['status']
        instance.save()

        return instance


class ElemenKenalanSerializer(serializers.ModelSerializer):
    is_approved = serializers.BooleanField()

    class Meta:
        model = Kenalan
        fields = ['rejection_message', 'is_approved']
    
    def update(self, instance, validated_data):
        if instance.status == KenalanStatus.ACCEPTED.name:
            raise serializers.ValidationError('You cannot edit kenalan that is already accepted!')
        elif instance.status == KenalanStatus.SAVED.name or instance.status == KenalanStatus.NOT_FRIEND.name:
            raise serializers.ValidationError('You cannot edit kenalan that is not yet submitted!')

        if validated_data['is_approved']:
            instance.status = KenalanStatus.ACCEPTED.name
        else:
            instance.rejection_message = validated_data['rejection_message']
            instance.status = KenalanStatus.REJECTED.name

        instance.save()

        return instance


class GetKenalanSerializer(serializers.ModelSerializer):
    user_maba = serializers.SerializerMethodField()
    user_elemen = serializers.SerializerMethodField()

    class Meta:
        model = Kenalan
        fields = '__all__'
        read_only_fields = ('detail_kenalan', 'user_elemen', 'user_maba')

    def get_user_maba(self, obj):
        serialized_maba = MabaSerializer(obj.user_maba.profil.maba)

        return serialized_maba.data
    
    def get_user_elemen(self, obj):
        serialized_elemen = ElemenSerializer(obj.user_elemen.profil.elemen)

        return serialized_elemen.data


class KenalanTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = KenalanTask
        fields = '__all__'


class KenalanUserStatisticsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    kenalan_task = KenalanTaskSerializer()
    
    class Meta:
        model = KenalanUserStatistics
        fields = '__all__'
