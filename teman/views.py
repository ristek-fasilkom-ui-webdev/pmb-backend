from akun.constants import ANGKATAN_MABA
from akun.models import Profil
from akun.serializers import UserDetailSerializer
from akun.permissions import IsMaba, IsElemen, IsOwner, is_maba, is_elemen
from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.utils.crypto import get_random_string
from teman.serializers import TokenSerializer, GetKenalanSerializer, MabaKenalanSerializer, ElemenKenalanSerializer, KenalanTaskSerializer, KenalanUserStatisticsSerializer
from teman.models import Token, Kenalan, KenalanTask, KenalanUserStatistics, KenalanStatus
from teman.utils import delete_expired_and_used_token, delete_expired_token
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes

class TokenView(APIView):
    permission_classes = [IsElemen]

    def get_token(self):
        token = get_random_string(length=6, allowed_chars='1234567890')
        if Token.objects.filter(token=token).exists():
            return self.get_token()
        return token

    def get(self, request):
        user = request.user
        count = request.GET.get('count', None)
        delete_expired_token(user)

        if Token.objects.filter(user=user).exists():
            token_obj = Token.objects.get(user=user)
            serialized_token = TokenSerializer(token_obj)
            return Response(serialized_token.data, status=status.HTTP_200_OK)
        elif count is not None:
            count = int(count)
            if count < 1 or count > 15:
                return Response(data={'Detail': 'Count must be in range 1 to 15'}, status=status.HTTP_400_BAD_REQUEST)
            token = self.get_token()
            token_obj = Token.objects.create(token=token, user=user, count=count)
            serialized_token = TokenSerializer(token_obj)
            return Response(serialized_token.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data={'Detail': 'Expired/used token, please regenerate'}, status=status.HTTP_404_NOT_FOUND)


class KenalanList(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        user = request.user
        if is_maba(user):
            kenalan_objects = Kenalan.objects.filter(user_maba=user)
        elif is_elemen(user):
            kenalan_objects = Kenalan.objects.filter(user_elemen=user)
        
        serialized_kenalan = GetKenalanSerializer(kenalan_objects, many=True)

        return Response(serialized_kenalan.data)

    def post(self, request):
        permission_classes = [IsMaba]

        with transaction.atomic():
            token_obj = get_object_or_404(Token.objects.select_for_update(), token=request.data['token'])
            
            if token_obj.end_time < timezone.now():
                delete_expired_token(token_obj.user)
                return Response({'detail': 'Token Expired'}, status=status.HTTP_400_BAD_REQUEST)

            user_maba = request.user
            user_elemen = token_obj.user

            if Kenalan.objects.filter(user_maba=user_maba, user_elemen=user_elemen).exists():
                kenalan_obj = Kenalan.objects.get(user_maba=user_maba, user_elemen=user_elemen)
                serialized_kenalan = GetKenalanSerializer(kenalan_obj)

                return Response(serialized_kenalan.data, status=status.HTTP_200_OK)
            else:
                if token_obj.count <= 0:
                    return Response({'detail': 'Token used up'}, status=status.HTTP_400_BAD_REQUEST)
                token_obj.count = F('count') - 1
                token_obj.save()
                token_obj.refresh_from_db()
                
                kenalan_obj = Kenalan.objects.create(user_maba=user_maba, user_elemen=user_elemen)
                serialized_kenalan = GetKenalanSerializer(kenalan_obj)

                return Response(serialized_kenalan.data, status=status.HTTP_201_CREATED)


class KenalanDetail(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk):
        user = request.user

        is_owner_as_maba = Kenalan.objects.filter(id=pk, user_maba=user).exists()
        is_owner_as_elemen = Kenalan.objects.filter(id=pk, user_elemen=user).exists()
        if is_owner_as_maba or is_owner_as_elemen:
            kenalan_obj = Kenalan.objects.get(id=pk)
            serialized_kenalan = GetKenalanSerializer(kenalan_obj)
            return Response(serialized_kenalan.data)
        return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

    def patch(self, request, pk):
        user = request.user

        is_owner_as_maba = Kenalan.objects.filter(id=pk, user_maba=user).exists()
        is_owner_as_elemen = Kenalan.objects.filter(id=pk, user_elemen=user).exists()
        if is_owner_as_maba or is_owner_as_elemen:
            kenalan_obj = Kenalan.objects.get(id=pk)
            if is_owner_as_maba:
                serialized_data = MabaKenalanSerializer(kenalan_obj, data=request.data, partial=True)
            else:
                serialized_data = ElemenKenalanSerializer(kenalan_obj, data=request.data, partial=True)

            if serialized_data.is_valid():
                updated_kenalan_obj = serialized_data.save()
                serialized_updated_kenalan_obj = GetKenalanSerializer(updated_kenalan_obj)
                return Response(serialized_updated_kenalan_obj.data)
            return Response(serialized_data.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

    def delete(self, request, pk):
        user = request.user

        is_owner_as_maba = Kenalan.objects.filter(id=pk, user_maba=user).exists()
        if is_owner_as_maba:
            kenalan_obj = Kenalan.objects.get(id=pk)

            if kenalan_obj.status == KenalanStatus.ACCEPTED.name or kenalan_obj.status == KenalanStatus.PENDING.name \
                or kenalan_obj.status == KenalanStatus.REJECTED.name:
                return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

            kenalan_obj.status = KenalanStatus.NOT_FRIEND.name
            kenalan_obj.description = ""
            kenalan_obj.rejection_message = ""

            kenalan_obj.save()
            
            return Response({'detail': 'Successfully set to NOT_FRIEND'}, status=status.HTTP_204_NO_CONTENT)
        return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

class KenalanTaskList(generics.ListAPIView):
    queryset = KenalanTask.objects.all()
    serializer_class = KenalanTaskSerializer
    permission_classes = [permissions.IsAuthenticated]


class KenalanUserStatisticsList(generics.ListAPIView):
    serializer_class = KenalanUserStatisticsSerializer
    permission_classes = [IsMaba]

    def get_queryset(self):
        return KenalanUserStatistics.objects.filter(user=self.request.user)


class FindFriend(APIView):
    permission_classes = [IsMaba]

    def get(self, request):
        query = request.GET.get('interest', None)

        if query is None:
            return Response(data={'Detail': 'Please send non-empty query'}, status=status.HTTP_400_BAD_REQUEST)
        
        interest_name = '%%%s%%' % query
        user = request.user
        potential_friends = User.objects.raw(
            """
                SELECT U.*, P.*, A.* FROM auth_user AS U
                LEFT JOIN akun_profil AS P
                ON U.id = P.user_id
                LEFT JOIN akun_angkatan AS A
                ON A.id = P.angkatan_id
                WHERE P.id IN (
                    SELECT P.id FROM akun_profil AS P
                    LEFT JOIN akun_interest AS I 
                    ON I.profil_id = P.id 
                    WHERE I.interest_name ILIKE %s
                ) AND NOT EXISTS (
                    SELECT K.id FROM teman_kenalan AS K
                    WHERE K.user_elemen_id = U.id
                    AND K.user_maba_id = %s
                ) AND A.tahun != %s
                AND P.is_private = FALSE
                ORDER BY RANDOM()
                LIMIT 30
            """, [interest_name, user.id, ANGKATAN_MABA]
        )

        serialized_potential_friends = UserDetailSerializer(potential_friends, many=True)
        return Response(serialized_potential_friends.data, status=status.HTTP_200_OK)