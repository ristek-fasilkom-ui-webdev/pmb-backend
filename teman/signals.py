from akun.models import Maba
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone
from situs.models import Notification, NotificationType
from teman.models import Kenalan, KenalanStatus, KenalanTask, KenalanUserStatistics
from teman.utils import update_kenalan_statistic_for_user_maba

@receiver(post_save, sender=Maba)
def maba_create_or_update(sender, created, instance, **kwargs):
    if created:
        kenalan_tasks = KenalanTask.objects.all()
        user = instance.profil.user
        for task in kenalan_tasks:
            KenalanUserStatistics.objects.create(user=user, kenalan_task=task)


@receiver(post_save, sender=KenalanTask)
def create_kenalan_user_statistics_when_kenalan_task_created(sender, created, instance, **kwargs):
    if created:
        kenalan_task = instance
        maba_list = Maba.objects.all()

        for maba in maba_list:
            user = maba.profil.user
            user_statistic = KenalanUserStatistics.objects.create(user=user, kenalan_task=kenalan_task)
            update_kenalan_statistic_for_user_maba(user_statistic, user)


@receiver(post_save, sender=Kenalan)
def update_kenalan_user_statistics_when_kenalan_created_or_updated(sender, created, instance, **kwargs):
    user_maba = instance.user_maba
    user_statistics = KenalanUserStatistics.objects.filter(user=user_maba)
    for user_statistic in user_statistics:
        kenalan_task = user_statistic.kenalan_task
        now = timezone.now()
        if kenalan_task.end_date >= now and kenalan_task.start_date <= now:
            update_kenalan_statistic_for_user_maba(user_statistic, user_maba)


@receiver(post_save, sender=Kenalan)
def when_kenalan_status_change_send_notification(sender, created, instance, **kwargs):
    if not created:
        user_maba = instance.user_maba
        user_elemen = instance.user_elemen
        status = instance.status
        if status == KenalanStatus.PENDING.name:
            notification = Notification.objects.create(
                user=user_elemen, 
                notification_type=NotificationType.KENALAN_PENDING.name, 
                related_object_type=ContentType.objects.get(app_label='teman', model='kenalan'),
                related_object_id=instance.id,
                is_viewed=False
            )
        elif status == KenalanStatus.REJECTED.name:
            notification = Notification.objects.create(
                user=user_maba, 
                notification_type=NotificationType.KENALAN_REJECTED.name, 
                related_object_type=ContentType.objects.get(app_label='teman', model='kenalan'),
                related_object_id=instance.id,
                is_viewed=False
            )
        elif status == KenalanStatus.ACCEPTED.name:
            notification = Notification.objects.create(
                user=user_maba, 
                notification_type=NotificationType.KENALAN_ACCEPTED.name, 
                related_object_type=ContentType.objects.get(app_label='teman', model='kenalan'),
                related_object_id=instance.id,
                is_viewed=False
            )