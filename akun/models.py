from django.db import models
from django.contrib.auth.models import User
from enum import Enum


class Angkatan(models.Model):
    tahun = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)

    class Meta:
        ordering = ['tahun']
        verbose_name = 'Angkatan'
        verbose_name_plural = 'Angkatan'

    def __str__(self):
        return self.nama


class Profil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profil")
    nama_lengkap = models.CharField(max_length=128)
    npm = models.CharField(max_length=10)
    about = models.TextField(blank=True, null=True)
    angkatan = models.ForeignKey(Angkatan, on_delete=models.SET_NULL, null=True)
    jurusan = models.CharField(max_length=50, blank=True, null=True)
    foto = models.URLField(blank=True, null=True)
    email = models.EmailField(max_length=256, blank=True, null=True)
    no_telepon = models.CharField(max_length=50, blank=True, null=True)
    tempat_lahir = models.CharField(max_length=50, blank=True, null=True)
    tanggal_lahir = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    asal_sekolah = models.CharField(max_length=50, blank=True, null=True)
    line_id = models.CharField(max_length=50, blank=True, null=True)
    linkedin = models.CharField(max_length=50, blank=True, null=True)
    instagram_id = models.CharField(max_length=50, blank=True, null=True)
    github_id = models.CharField(max_length=50, blank=True, null=True)
    other_website = models.CharField(max_length=50, blank=True, null=True)
    is_private = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['nama_lengkap']
        verbose_name = 'Profil'
        verbose_name_plural = 'Profil'

    def __str__(self):
        return self.nama_lengkap


class KelompokBesar(models.Model):
    nama = models.CharField(max_length=50)

    class Meta:
        ordering = ['nama']
        verbose_name = 'Kelompok Besar'
        verbose_name_plural = 'Kelompok Besar'


    def __str__(self):
        return self.nama
    
    
class KelompokKecil(models.Model):
    nama = models.CharField(max_length=50)

    class Meta:
        ordering = ['nama']
        verbose_name = 'Kelompok Kecil'
        verbose_name_plural = 'Kelompok Kecil'

    def __str__(self):
        return self.nama


class PasanganKelompok(models.Model):
    kelompok_besar = models.ForeignKey(KelompokBesar, on_delete=models.CASCADE)
    kelompok_kecil = models.ForeignKey(KelompokKecil, on_delete=models.CASCADE)

    class Meta:
        ordering = ['kelompok_besar', 'kelompok_kecil']
        verbose_name = 'Pasangan Kelompok'
        verbose_name_plural = 'Pasangan Kelompok'

    def __str__(self):
        return str(self.kelompok_besar) + ' - ' + str(self.kelompok_kecil)

class Maba(models.Model):
    profil = models.OneToOneField(Profil, on_delete=models.CASCADE, related_name="maba")
    kelompok = models.ForeignKey(PasanganKelompok, on_delete=models.SET_NULL, blank=True, null=True)
    pengoreksi = models.CharField(max_length=128, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['profil__nama_lengkap']
        verbose_name = 'Maba'
        verbose_name_plural = 'Maba'

    def __str__(self):
        return str(self.profil)

class Elemen(models.Model):
    profil = models.OneToOneField(Profil, on_delete=models.CASCADE, related_name="elemen")
    title = models.CharField(max_length=128, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['profil__nama_lengkap']
        verbose_name = 'Elemen'
        verbose_name_plural = 'Elemen'

    def __str__(self):
        return str(self.profil)


class InterestCategory(Enum):
    TECHNOLOGY = 'Technology'
    NON_TECH = 'Non-Tech'
    ENTERTAINMENT = 'Entertainment'

    @staticmethod
    def get_default():
        return InterestCategory.TECHNOLOGY.value


class Interest(models.Model):
    profil = models.ForeignKey(Profil, related_name="interests", on_delete=models.CASCADE)
    interest_name = models.CharField(max_length=255)
    interest_category = models.CharField(max_length=255, choices=[(category.value, category.value) for category in InterestCategory], default=InterestCategory.get_default())
    is_top_interest = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['profil', 'interest_category', '-is_top_interest']
        verbose_name = 'Interest'
        verbose_name_plural = 'Interests'

    def __str__(self):
        return self.profil.nama_lengkap + " is interested with " + self.interest_name
