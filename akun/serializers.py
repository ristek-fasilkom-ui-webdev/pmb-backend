from django.contrib.auth.models import User
from rest_framework import serializers
from akun.models import Angkatan, Profil, KelompokBesar, KelompokKecil, PasanganKelompok, Maba, Elemen, Interest


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username',)
        read_only_fields = ('id', 'username',)


class AngkatanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Angkatan
        fields = '__all__'
        read_only_fields = ('tahun', 'nama',)


class EmbeddedInterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = ['interest_name', 'interest_category', 'is_top_interest']
        read_only_fields = ['interest_name', 'interest_category', 'is_top_interest']


class ProfilSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    angkatan = AngkatanSerializer()
    interests = serializers.SerializerMethodField()

    class Meta:
        model = Profil
        fields = '__all__'
        read_only_fields = ('nama', 'npm', 'angkatan', 'email', 'created_at', 'updated_at',)

    def get_interests(self, obj):
        interests = obj.interests
        serialized_interests = EmbeddedInterestSerializer(interests, many=True)

        return serialized_interests.data


class MorePrivateProfilSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    angkatan = AngkatanSerializer()

    class Meta:
        model = Profil
        fields = ['user', 'nama_lengkap', 'about', 'angkatan', 'jurusan', 'foto', 'created_at', 'updated_at']


class KelompokBesarSerializer(serializers.ModelSerializer):
    class Meta:
        model = KelompokBesar
        fields = '__all__'
        read_only_fields = ('nama',)


class KelompokKecilSerializer(serializers.ModelSerializer):
    class Meta:
        model = KelompokKecil
        fields = '__all__'
        read_only_fields = ('nama',)


class PasanganKelompokSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    kelompok_besar = KelompokBesarSerializer()
    kelompok_kecil = KelompokKecilSerializer()

    class Meta:
        model = PasanganKelompok
        fields = '__all__'
        read_only_fields = ('kelompok_besar', 'kelompok_kecil',)


class MabaSerializer(serializers.ModelSerializer):
    profil = ProfilSerializer()
    kelompok = PasanganKelompokSerializer()

    class Meta:
        model = Maba
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at',)
    
    def update(self, instance, validated_data):
        profil_data = validated_data.get('profil')
        if profil_data is not None:
            profil_serializer = ProfilSerializer(instance.profil, data=profil_data, partial=True)
            if profil_serializer.is_valid():
                instance.profil = profil_serializer.save()

        kelompok_data = validated_data.get('kelompok')
        if kelompok_data is not None:
            kelompok_object = PasanganKelompok.objects.get(id=kelompok_data['id'])
            instance.kelompok = kelompok_object
        instance.save()

        return instance


class ElemenSerializer(serializers.ModelSerializer):
    profil = ProfilSerializer()

    class Meta:
        model = Elemen
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at',)

    def update(self, instance, validated_data):
        profil_data = validated_data.get('profil')
        if profil_data is not None:
            profil_serializer = ProfilSerializer(instance.profil, data=profil_data, partial=True)
            if profil_serializer.is_valid():
                instance.profil = profil_serializer.save()

        instance.title = validated_data.get('title', instance.title)
        instance.save()

        return instance


class UserDetailSerializer(serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'username', 'user_detail']
        read_only_fields = ['id', 'username', 'user_detail']
    
    def get_user_detail(self, obj):
        profil_obj = obj.profil
        
        try:
            maba_obj = profil_obj.maba
        except Maba.DoesNotExist:
            maba_obj = None

        try:
            elemen_obj = profil_obj.elemen
        except Elemen.DoesNotExist:
            elemen_obj = None
        
        if maba_obj is not None:
            serialized_obj = MabaSerializer(maba_obj)
        elif elemen_obj is not None:
            serialized_obj = ElemenSerializer(elemen_obj)
        else:
            return None
        
        return serialized_obj.data


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = '__all__'
        read_only_fields = ['created_at', 'updated_at']


class GetInterestSerializer(serializers.ModelSerializer):
    profil = MorePrivateProfilSerializer()

    class Meta:
        model = Interest
        fields = '__all__'
