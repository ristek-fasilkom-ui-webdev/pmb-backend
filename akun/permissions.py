from rest_framework import permissions
from akun.models import Profil, Maba, Elemen


def is_maba(user):
    profil = Profil.objects.filter(user=user)
    if (profil.count() == 1):
        return Maba.objects.filter(profil=profil[0]).exists()
    return False

def is_elemen(user):
    profil = Profil.objects.filter(user=user)
    if (profil.count() == 1):
        return Elemen.objects.filter(profil=profil[0]).exists()
    return False


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        try:
            if hasattr(obj, 'user'):
                return obj.user == request.user
            elif hasattr(obj, 'author_id'):
                return obj.author_id == request.user.id
        except Exception as e:
            return False


class IsElemen(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return is_elemen(request.user)

    def has_permission(self, request, view):
        return is_elemen(request.user)


class IsMaba(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return is_maba(request.user)

    def has_permission(self, request, view):
        return is_maba(request.user)