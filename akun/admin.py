from django.contrib import admin
from akun.models import Angkatan, Profil, KelompokBesar, KelompokKecil, PasanganKelompok, Maba, Elemen, Interest


class AngkatanModelAdmin(admin.ModelAdmin):
    list_display = ['nama', 'tahun']
    search_fields = ['nama', 'tahun']

    class Meta:
        model = Angkatan


class ProfilModelAdmin(admin.ModelAdmin):
    list_display = ['nama_lengkap', 'npm', 'angkatan', 'user']
    search_fields = ['nama_lengkap', 'npm', 'user__username']
    list_filter = ['angkatan']

    class Meta:
        model = Profil


class PasanganKelompokModelAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'kelompok_besar', 'kelompok_kecil']
    list_filter = ['kelompok_besar']

    class Meta:
        model = PasanganKelompok


class MabaModelAdmin(admin.ModelAdmin):
    list_display = ['profil', 'kelompok']
    search_fields = ['profil__nama_lengkap', 'kelompok__kelompok_besar__nama', 'kelompok__kelompok_kecil__nama']
    list_filter = ['kelompok']

    class Meta:
        model = Maba


class ElemenModelAdmin(admin.ModelAdmin):
    list_display = ['profil', 'title']
    search_fields = ['profil__nama_lengkap']

    class Meta:
        model = Elemen


admin.site.register(Angkatan, AngkatanModelAdmin)
admin.site.register(Profil, ProfilModelAdmin)
admin.site.register(KelompokBesar)
admin.site.register(KelompokKecil)
admin.site.register(PasanganKelompok, PasanganKelompokModelAdmin)
admin.site.register(Maba, MabaModelAdmin)
admin.site.register(Elemen, ElemenModelAdmin)
admin.site.register(Interest)
