EMAIL_DOMAIN = '@ui.ac.id'

PREFIX_TAHUN = '20' #In case this codebase somehow survive until the 22nd century

ANGKATAN_AKTIF = ['2016', '2017', '2018']
ANGKATAN_MABA = '2019'

NAMA_ANGKATAN_NON_AKTIF = 'Elemen'
NAMA_ANGKATAN = {
    '2016': 'Omega',
    '2017': 'Tarung',
    '2018': 'Quanta',
    '2019': 'Maba',
}
