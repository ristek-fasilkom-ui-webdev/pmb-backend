from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, status, permissions, mixins
from akun.models import Angkatan, Profil, KelompokBesar, KelompokKecil, PasanganKelompok, Maba, Elemen, Interest
from akun.serializers import MabaSerializer, ElemenSerializer, PasanganKelompokSerializer, InterestSerializer, GetInterestSerializer
from akun.permissions import is_maba, is_elemen, IsOwner


class ProfilListView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        maba = Maba.objects.all()
        serialized_maba = MabaSerializer(maba, many=True)

        elemen = Elemen.objects.all()
        serialized_elemen = ElemenSerializer(elemen, many=True)

        return Response(serialized_maba.data + serialized_elemen.data)

class ProfilDetailView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, id):
        user = get_object_or_404(User, id=id)
        profil = get_object_or_404(Profil, user=user)

        if is_maba(user):
            maba = Maba.objects.get(profil=profil)
            serialized_maba = MabaSerializer(maba)
            return Response(serialized_maba.data)
        else:
            elemen = Elemen.objects.get(profil=profil)
            serialized_elemen = ElemenSerializer(elemen)
            return Response(serialized_elemen.data)

class MyProfilView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    
    def get(self, request):
        profil = get_object_or_404(Profil, user=request.user)

        if is_maba(request.user):
            maba = Maba.objects.get(profil=profil)
            serialized_maba = MabaSerializer(maba)
            return Response(serialized_maba.data)
        else:
            elemen = Elemen.objects.get(profil=profil)
            serialized_elemen = ElemenSerializer(elemen)
            return Response(serialized_elemen.data)

    def patch(self, request):
        self.permission_classes = [IsOwner]
        profil = get_object_or_404(Profil, user=request.user)
        if is_maba(request.user):
            maba = get_object_or_404(Maba, profil=profil)
            serializer = MabaSerializer(maba, data=request.data, partial=True)
            if serializer.is_valid():
                updated_profil = serializer.save()
                serialized_update_profil = MabaSerializer(updated_profil)
                return Response(serialized_update_profil.data)
        else:
            elemen = get_object_or_404(Elemen, profil=profil)
            serializer = ElemenSerializer(elemen, data=request.data, partial=True)
            if serializer.is_valid():
                updated_profil = serializer.save()
                serialized_updated_profil = ElemenSerializer(updated_profil)
                return Response(serialized_updated_profil.data)
        return Response(data='Bad Request', status=status.HTTP_400_BAD_REQUEST)


class PasanganKelompokListView(generics.ListAPIView):
    queryset = PasanganKelompok.objects.all()
    serializer_class = PasanganKelompokSerializer
    permission_classes = [permissions.IsAuthenticated]


class InterestListView(generics.ListCreateAPIView):
    serializer_class = GetInterestSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        query = self.request.GET.get('user', None)

        if query is not None:
            requested_user = User.objects.get(id=query)
            return Interest.objects.filter(profil=requested_user.profil)
        return Interest.objects.all()
    
    def create(self, request, *args, **kwargs):
        data = request.data
        data['profil'] = request.user.profil.id
        serializer = InterestSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        created_interest = serializer.save()
        response_serializer = GetInterestSerializer(created_interest)
        return Response(response_serializer.data, status=status.HTTP_201_CREATED)


class InterestDetailView(generics.RetrieveDestroyAPIView):
    serializer_class = GetInterestSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return Interest.objects.filter(profil=self.request.user.profil)
    
    def patch(self, request, pk):
        interest_obj = get_object_or_404(Interest, id=pk)
        if interest_obj.profil == request.user.profil:
            Interest.objects.filter(profil=request.user.profil, interest_category=interest_obj.interest_category).update(is_top_interest=False)
            interest_obj.is_top_interest = True
            interest_obj.save()

            interest_list = Interest.objects.filter(profil=self.request.user.profil)
            response_serializer = GetInterestSerializer(interest_list, many=True)
            return Response(response_serializer.data, status=status.HTTP_200_OK)
        return Response(data={'detail': 'Not Found'}, status=status.HTTP_404_NOT_FOUND)
