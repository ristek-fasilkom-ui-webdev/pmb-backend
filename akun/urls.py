from django.urls import re_path
from rest_framework_jwt.views import obtain_jwt_token
from akun import cas_views
from akun.views import ProfilListView, ProfilDetailView, MyProfilView, PasanganKelompokListView, InterestListView, InterestDetailView

urlpatterns = [
    re_path(r'^login$', cas_views.login, name='cas_ng_login'),
    re_path(r'^logout$', cas_views.logout, name='cas_ng_logout'),
    re_path(r'^profil$', ProfilListView.as_view(), name='profil_list'), # Closed until find friends feature is released
    re_path(r'^profil/(?P<id>[0-9]+)$', ProfilDetailView.as_view(), name='profil_detail'),
    re_path(r'^profil/me$', MyProfilView.as_view(), name='profil_saya'),
    re_path(r'^kelompok$', PasanganKelompokListView.as_view(), name='kelompok_list'),
    re_path(r'^interests$', InterestListView.as_view(), name='interest_list'),
    re_path(r'^interests/(?P<pk>[0-9]+)$', InterestDetailView.as_view(), name='interest_detail'),
    re_path(r'^callback$', cas_views.callback, name='cas_ng_proxy_callback'),
    re_path(r'^api-token-auth$', obtain_jwt_token),
]
