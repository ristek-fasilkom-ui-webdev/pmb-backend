from akun.constants import EMAIL_DOMAIN, PREFIX_TAHUN, NAMA_ANGKATAN_NON_AKTIF, NAMA_ANGKATAN


def generate_email_from_username(username):
    return username.strip() + EMAIL_DOMAIN

def get_angkatan_from_npm(npm):
    suffix = npm[:2]
    return PREFIX_TAHUN + suffix

def get_nama_angkatan_from_tahun_angkatan(tahun_angkatan):
    if tahun_angkatan in NAMA_ANGKATAN:
        return NAMA_ANGKATAN[tahun_angkatan]
    
    return NAMA_ANGKATAN_NON_AKTIF
