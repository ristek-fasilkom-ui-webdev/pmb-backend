# PMB Backend

## Developers Guide

### Sections

- [PMB Backend](#PMB-Backend)
  - [Developers Guide](#Developers-Guide)
    - [Sections](#Sections)
    - [Installation, Usage, and Conduct Guidelines](#Installation-Usage-and-Conduct-Guidelines)
      - [Installation](#Installation)
      - [For Backend](#For-Backend)
    - [Tugas](#Tugas)
      - [Tugas Task](#Tugas-Task)
      - [Tugas Event](#Tugas-Event)
      - [**Log Tugas**](#Log-Tugas)
    - [Daftar Apps](#Daftar-Apps)
  - [API Guide](#API-Guide)
    - [Endpoints](#Endpoints)
      - [Login and Logout](#Login-and-Logout)
      - [Profil](#Profil)
      - [Kelompok](#Kelompok)
      - [File Upload](#File-Upload)
      - [Event](#Event)
      - [Announcement](#announcement)
      - [Task and Submission](#task-and-submission)
      - [Question and Answers](#qna)
      - [Server Time](#server-time)
      - [Kenalan](#kenalan)
      - [Interest](#interest)
      - [Story](#story)
      - [Notification](#notification)
      - [Find Friends](#find-friends)
      - [Kata Elemen](#kata-elemen)

### Installation, Usage, and Conduct Guidelines

[Back to sections](#sections)

Pastikan anda telah menginstall

- python3.7
- pipenv (**`pip install pipenv`**)
- PostgreSQL (nanti, sekarang belum)

#### Installation

- Please don't skip above steps (except PostgreSQL, you can skip that)
- Di terminal, jalankan `pipenv sync`
- Jalankan `pipenv shell`
- Copy file bernama .env-default, lalu rename menjadi .env
- Jalankan `python manage.py makemigrations` dan `python manage.py migrate`
- Jalankan `python manage.py runserver`, backend bisa diakses di [http://localhost:8000](http://localhost:8000)
- If something went FUBAR, chat Cahya (langsung chat apa masalahnya dan preferably with screenshot, Cahya biasanya kalo ada yang chat 'Cah' aja balesnya lama)

#### For Backend

Basic pipenv commands :

- **`pipenv shell`** Mengaktifkan virtual environment
- **`pipenv sync`** : Menginstall semua package yang diperlukan di virtual environment
- **`pipenv install <nama-package>`** : Menambahkan package baru ke virtual environment

Basic Django usage :

- Ingat makemigrations
- Ingat migrate
- [Documentation Django](https://docs.djangoproject.com/en/2.2/)

Conduct Guidelines :

- Aktifkan virtual environment sebelum bekerja
- Letakkan environment variables yang sensitif di file .env (contoh password database). File .env-default berisi daftar nama env variables yang digunakan di proyek ini.
- Commit sewajarnya! Jangan terlalu banyak, jangan terlalu sedikit. Rule of thumb : Kelar suatu fitur/perubahan settings/fix satu bug, commit
- **BIKIN BRANCH PUNYA SENDIRI. COMMIT DI SANA. PUSH KE SANA. MERGE REQUEST KE MASTER DI GITLAB. JANGAN LANGSUNG COMMIT, PUSH, ATAU MERGE KE MASTER TANPA IJIN EKSPLISIT.** (Actually idealnya per fitur satu branch tapi juga yang ngerjain cuma sendiri [*ehem senmem kan hanya monitor hehe*])
- Good commit message. Tidak perlu formal, yang penting menjelaskan ini commit melakukan apa.
- `"Google Sana, Google Sini, Coba Itu, Coba Ini, Lalu Tanya-tanyi"` - Our Lord and Savior, Bapak Rahmat M. Samik Ibrahim
- [Kitab Backend PMB](https://github.com/webdevxpmb/backend)

Conventions :

- **Apps** menggunakan bahasa **Indonesia**
- Kode belum ditentukan akan menggunakan bahasa apa (I'm partial to english, maybe nanti bebas)
- Use DRF Class View
- We prefer blank=True, null=True for char or text fields (**EDITED**)
- **SEKARANG ENGLISH ONLY YA**

### Tugas

[Back to sections](#sections)

**This section is updated at 7 July 2019**
Untuk 2 minggu dari tanggal yang dituliskan di atas, berikut adalah to-do-list yang harus dikerjakan (Prioritas yang dituliskan dibawah independen dari prioritas yang diberikan oleh pihak PMB. Konteks prioritas mengacu pada 2 minggu pengerjaan, bukan proyek secara keseluruhan )

1. [Task](#tugas-task)
2. [Event](#tugas-event)

#### Tugas Task

Menampilkan list tugas yang dipublikasikan. Setiap item tugas terdiri dari:

- Judul Tugas
- Deadline
- Deskripsi Tugas
- File/Link Lampiran
- Question and Answer [LM]
- Submission [Maba]
- Submission memiliki waktu deadline [H] dan cut-off [M] (seperti scele).

CRUD Guide (Create, Read, Delete, Update) :

- **CREATED** by admin
- Can be **READ** by everyone
- **DELETE** tidak perlu dihandle
- Can be **UPDATED** by admin

**Note from Senior**

Pikirkan gimana ngemodelin Task, Question and Answer, sama Submissionnya

Junior bikin Deskripsi Task dan QA, Senior handle Submissionnya

#### Tugas Event

Menampilkan list event yang akan dipublikasikan dan sudah dipublikasikan. Fitur ini ada untuk mahasiswa baru dan mahasiswa lama. Fitur ini berupa satu halaman saja yang terdiri dari 2 bagian, yaitu upcoming event dan past event. Untuk tampilannya, kurang lebih mirip seperti event yang ada di website PMB lama.

Baca [Kitab](https://github.com/webdevxpmb/backend) as usual

**Note from Senior**

This should be very easy. Cek [mockup](https://www.figma.com/file/doaHgURGQBXaEUiDDu41VlPc/Mockup-Website-PMB-2019?node-id=398%3A6405) untuk detail fieldnya

#### **Log Tugas**

- Tugas Login dan Logout (**DONE**)

- Tugas Profil User (**PARTIALLY DONE**)

- Tugas Task (**NOT DONE :|**)

- Tugas Event (**NOT DONE :|**)

### Daftar Apps

- akun : Mengatur semua hal yang berhubungan dengan akun seperti login, logout, profil, dan badges
- situs : Mengatur hal-hal simple yang relatif statis akan ditampilkan di frontend, seperti task, event, announcement, dsb.
- teman : Mengatur hal-hal seperti kenalan, interest, find friends, dsb.

## API Guide

[Back to sections](#sections)

### Endpoints

Remember to do [this](#installation) first!

1. [Login and Logout](#login-and-logout)
2. [File Upload](#file-upload)
3. [Profil](#profil)
4. [Event](#event)
5. [Announcement](#announcement)
6. [Task and Submission](#task-and-submission)

#### Login and Logout

- Login
  > /akun/login

  User akan diarahkan ke halaman SSO UI, lalu backend akan menampilkan halaman kosong yang melakukan post event dengan nama **message**. Post event ini akan mengirimkan data dengan format berikut :

  ```json
  token: "{{ token }}",
  user_id:"{{ user_id }}",
  username: "{{ user }}",
  profile_id: "{{ profile_id }}",
  nama_lengkap: "{{ nama_lengkap }}",
  npm: "{{ npm }}",
  email: "{{ email }}",
  angkatan: "{{ angkatan }}",
  ```

  Bagaimana cara frontend mendapatkan informasi dari post event ini? Frontend akan menambahkan sebuah **Event Listener** yang menunggu sebuah post bernama **message**. Kemudian data Post event akan diterima dan diproses.

- Logout
  > /akun/logout

  Lakukan GET request ke URL diatas. User akan dilogout dari backend dan diarahkan kembali ke halaman frontend (by default [http://localhost:3000](http://localhost:3000), I haven't implemented an more elegant way that make it easier for you frontend guys to change this redirect URL).

  Frontend akan menghapus token JWT dari local storage saat melakukan logout.

- Notes on Token
  Ketika token expired, maka akan mengembalikan status `401` dengan message berikut : `"detail": "Signature has expired."`

#### File Upload

- Upload file
  > POST /situs/upload

  Digunakan untuk upload file seperti foto profil atau submisi task.
  
  Upload menggunakan `form-data`. Key untuk filenya adalah `file`.

  Bila upload sukses, akan mengembalikan JSON berikut :
  ```json
  {
    "id": 3,
    "file": "http://localhost:8000/media/icebear.txt",
    "created_at": "2019-07-20T14:45:36.264690Z"}
  ```


#### Profil

- Profil User yang sedang Login
  > GET /akun/profil/me

  Digunakan untuk mendapatkan profil user yang sedang login.
  Untuk maba berbentuk seperti ini :
  ```json
  {
    "id": 1,
    "profil": {
        "id": 4,
        "user": {
            "id": 12,
            "username": "pande.ketut71"
        },
        "angkatan": {
            "id": 1,
            "tahun": "2017",
            "nama": "Tarung"
        },
        "nama_lengkap": "Pande Ketut Cahya Nugraha",
        "npm": "1706028663",
        "about": "Sad Boi",
        "jurusan": "Ilmu Kesedihan",
        "foto": null,
        "email": "pande.ketut71@ui.ac.id",
        "no_telepon": null,
        "tempat_lahir": null,
        "tanggal_lahir": null,
        "asal_sekolah": null,
        "line_id": null,
        "linkedin": null,
        "instagram_id": null,
        "github_id": null,
        "other_website": null,
        "created_at": "2019-07-13T13:38:44.599590Z",
        "updated_at": "2019-07-13T13:38:44.599630Z"
    },
    "kelompok": {
        "id": 1,
        "kelompok_besar": {
            "id": 1,
            "nama": "Calypso"
        },
        "kelompok_kecil": {
            "id": 1,
            "nama": "TypeScript"
        }
    },
    "created_at": "2019-07-13T13:38:44.614687Z",
    "updated_at": "2019-07-13T14:30:12.060149Z"
  }
  ```

  Untuk elemen:
  ```json
  { 
    "id": 1,
    "profil": {
        "id": 4,
        "user": {
            "id": 12,
            "username": "pande.ketut71"
        },
        "angkatan": {
            "id": 1,
            "tahun": "2017",
            "nama": "Tarung"
        },
        "nama_lengkap": "Pande Ketut Cahya Nugraha",
        "npm": "1706028663",
        "about": "Happy Boi",
        "jurusan": "Ilmu Kesenangan",
        "foto": null,
        "email": "pande.ketut71@ui.ac.id",
        "no_telepon": null,
        "tempat_lahir": null,
        "tanggal_lahir": null,
        "asal_sekolah": null,
        "line_id": null,
        "linkedin": null,
        "instagram_id": null,
        "github_id": null,
        "other_website": null,
        "created_at": "2019-07-13T13:38:44.599590Z",
        "updated_at": "2019-07-13T13:38:44.599630Z"
    },
    "title": "Master Procrastinator",
    "created_at": "2019-07-13T13:38:44.614687Z",
    "updated_at": "2019-07-13T14:30:12.060149Z"
  }
  ```

  > PATCH /akun/profil/me

  Digunakan untuk mengubah profil user yang sedang login.
  Request Bodynya cukup sebagian dari data profil yang ditunjukkan diatas. Berikut merupakan beberapa contoh JSONnya :
  ```json
  {
    "id": 1,
    "profil": {
      "foto": "https://www.webarebears.com",
      "about": "WBB",
    }
  }
  ```
  ```json
  {
    "id": 1, /* Ini outermost ID ya, bukan ID dalem object profil */
    "profil": {
      "no_telepon": "081999749715",
      "tempat_lahir": "Denpasar",
      "tanggal_lahir": "12 April 1999"
    },
    "title": "Capek nih PP Duri - Depok :( Sekarang PP Sudirman - Depok"
  }
  ```
  ```json
  {
    "id": 1,
    "profil": {
      "foto": "http://localhost:8000/media/1485360295.minhpupu_183.jpg", /* Upload file ke /situs/upload-file terlebih dahulu untuk mendapatkan urlnya (see section File Upload) */
      "asal_sekolah": "SMAN 3 Denpasar",
    },
    "kelompok": {
      "id": 2 /* Untuk kelompok pake id dari pasangan kelompok kecil dan kelompok besarnya. Refer to section Kelompok. */
    }
  }
  ```

#### Kelompok
- List pilihan kelompok yang bisa dipilih maba
  > GET /akun/kelompok

  ```json
  [
    {
        "id": 1,
        "kelompok_besar": {
            "id": 1,
            "nama": "Calypso"
        },
        "kelompok_kecil": {
            "id": 1,
            "nama": "TypeScript"
        }
    },
    {
        "id": 2,
        "kelompok_besar": {
            "id": 1,
            "nama": "Calypso"
        },
        "kelompok_kecil": {
            "id": 2,
            "nama": "Gado-Gado"
        }
    }
  ]
  ```

#### File Upload
- Uploading file
  > POST /situs/upload-file

  Karena endpoint lain menggunakan JSON, maka bila ada kegiatan yang memerlukan upload file harus melalui endpoint yang berbeda.

  Untuk upload file, kirim file ke endpoint diatas method POST dan request body berupa `Form Data`, dengan `key` untuk file yang diupload adalah `file`.

  Akan mengembalikan response berupa url dari file yang telah diupload.
  ``` json
  {
    "id": 4,
    "file": "http://localhost:8000/media/1485360295.minhpupu_183.jpg",
    "created_at": "2019-08-03T08:31:17.475551Z"
  }
  ```

#### Event
- List event yang akan datang dan sudah berlangsung
  > GET /situs/events

  Menampilkan list event yang ada, seperti ini :
  ```json
  [
    {
        "id": 1,
        "title": "Main GO-PLAY",
        "date": "2019-08-07T22:44:52+07:00",
        "place": "Gojek HQ",
        "description": "Males bikin web PMB, mending main GO-PLAY",
        "attachment_link": null,
        "cover_image_link": null,
        "organizer": "Ristek",
        "organizer_image_link": null,
        "created_at": "2019-08-07T22:56:05.185248+07:00",
        "updated_at": "2019-08-07T22:56:05.194223+07:00"
    },
    {
        "id": 2,
        "title": "Meratapi diri sendiri",
        "date": "2019-08-07T22:44:52+07:00",
        "place": "Rumah atau kos masing-masing",
        "description": "Sad. Just sad",
        "attachment_link": "https://www.bustle.com/p/can-a-broken-heart-make-you-sick-its-more-dangerous-than-you-think-so-heres-how-to-heal-7415492",
        "cover_image_link": "https://i.kym-cdn.com/entries/icons/mobile/000/026/489/crying.jpg",
        "organizer": "Barisan Millenials Sedih",
        "organizer_image_link": null,
        "created_at": "2019-08-07T22:56:05.185248+07:00",
        "updated_at": "2019-08-07T22:56:05.194223+07:00"
    }
  ]
  ```

- Detail event yang akan datang dan sudah berlangsung
  > GET /situs/events/{id-event}

  Menampilkan detail dari suatu event, berupa JSON seperti berikut :
  > /situs/events/2
  ```json
  {
      "id": 2,
      "qnas": [
        {
          "user": {
              "id": 12,
              "username": "pande.ketut71",
              "user_detail": {
                  "id": 1,
                  "profil": {
                      "id": 4,
                      "user": {
                          "id": 12,
                          "username": "pande.ketut71"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "Pande Ketut Cahya Nugraha",
                      "npm": "1706028663",
                      "about": "http://localhost:8000/media/1485360295.minhpupu_183.jpg",
                      "jurusan": null,
                      "foto": null,
                      "email": "pande.ketut71@ui.ac.id",
                      "no_telepon": "123",
                      "tempat_lahir": "123",
                      "tanggal_lahir": "2019-08-03",
                      "asal_sekolah": "SMAN 3 Denpasar",
                      "line_id": "123",
                      "linkedin": "123",
                      "instagram_id": "123",
                      "github_id": "123",
                      "other_website": "123",
                      "created_at": "2019-07-13T20:38:44.599590+07:00",
                      "updated_at": "2019-08-10T13:25:10.830477+07:00"
                  },
                  "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
                  "created_at": "2019-08-20T23:22:01.400864+07:00",
                  "updated_at": "2019-08-20T23:22:01.400864+07:00"
              }
          },
          "comment": "<p>Original Wanted Now</p>",
          "created_at": "2019-08-07T22:45:42.135847+07:00",
          "updated_at": "2019-08-20T23:21:17.862224+07:00"
        }
      ],
      "title": "Meratapi diri sendiri",
      "date": "2019-08-07T22:44:52+07:00",
      "place": "Rumah atau kos masing-masing",
      "description": "Sad. Just sad",
      "attachment_link": "https://www.bustle.com/p/can-a-broken-heart-make-you-sick-its-more-dangerous-than-you-think-so-heres-how-to-heal-7415492",
      "cover_image_link": "https://i.kym-cdn.com/entries/icons/mobile/000/026/489/crying.jpg",
      "organizer": "Barisan Millenials Sedih",
      "organizer_image_link": null,
      "created_at": "2019-08-07T22:56:05.185248+07:00",
      "updated_at": "2019-08-07T22:56:05.194223+07:00"
  }
  ```

#### Announcement
- List announcement
  > GET /situs/announcements

  Menampilkan daftar announcement yang tersedia, dengan JSON sebagai berikut :
  ```json
  [
    {
        "id": 1,
        "title": "Announcement",
        "date": "2019-08-07T22:44:52.356048+07:00",
        "description": null,
        "attachment_link": null,
        "created_at": "2019-08-07T22:44:52.354053+07:00",
        "updated_at": "2019-08-07T22:44:52.368016+07:00"
    },
    {
        "id": 2,
        "title": "Cuma Pengumuman",
        "date": "2019-08-07T22:44:52.356048+07:00",
        "description": "Cahya bisa makan sapi guys",
        "attachment_link": "https://cahyanugraha12.com/lagi-makan-sapi.jpg",
        "created_at": "2019-08-07T22:44:52.354053+07:00",
        "updated_at": "2019-08-07T22:44:52.368016+07:00"
    }
  ]
  ```

- Detail announcement
  > GET /situs/announcements/{id-announcement}

  Menampilkan detail dari suatu announcement, berupa JSON seperti berikut :
  > /situs/announcements/2
  ```json
  {
      "id": 2,
      "qnas": [ /* See event detail */ ],
      "title": "Cuma Pengumuman",
      "date": "2019-08-07T22:44:52.356048+07:00",
      "description": "Cahya bisa makan sapi guys",
      "attachment_link": "https://cahyanugraha12.com/lagi-makan-sapi.jpg",
      "created_at": "2019-08-07T22:44:52.354053+07:00",
      "updated_at": "2019-08-07T22:44:52.368016+07:00"
  }
  ```

#### Task and Submission
- List task
  > GET /situs/tasks

  Menampilkan daftar task yang tersedia, dengan JSON sebagai berikut :
  ```json
  [
    {
        "id": 1,
        "is_submitted": false,
        "title": "Task",
        "start_date": "2019-08-07T22:44:52.466752+07:00",
        "end_date": "2019-08-03T17:58:16+07:00",
        "description": null,
        "attachment_link": null,
        "can_have_submission": false,
        "created_at": "2019-08-03T17:58:22.317358+07:00",
        "updated_at": "2019-08-03T17:58:22.317358+07:00",
    },
    {
        "id": 2,
        "is_submitted": true,
        "title": "Task : Menghibur diri sendiri",
        "start_date": "2019-08-07T22:44:52.466752+07:00",
        "end_date": "2019-08-20T18:33:32+07:00",
        "description": "Menghibur diri sendiri di rumah masing-masing. Sertakan video dirimu yang telah senang setelah menghibur diri sendiri",
        "attachment_link": "https://ga-usah-bucin-mending-menghibur-diri.com",
        "can_have_submission": true,
        "created_at": "2019-08-03T18:33:37.832038+07:00",
        "updated_at": "2019-08-03T18:33:54.625908+07:00",
    }
  ]
  ```

- Detail task
  > GET /situs/tasks/{id-task}

  Menampilkan detail dari suatu task, berupa JSON seperti berikut :
  > /situs/tasks/2
  ```json
  {
      "id": 2,
      "is_submitted": false,
      "qnas": [ /* See event detail */ ],
      "title": "Task : Menghibur diri sendiri",
      "start_date": "2019-08-07T22:44:52.466752+07:00",
      "end_date": "2019-08-20T18:33:32+07:00",
      "description": "Menghibur diri sendiri di rumah masing-masing. Sertakan video dirimu yang telah senang setelah menghibur diri sendiri",
      "attachment_link": "https://ga-usah-bucin-mending-menghibur-diri.com",
      "can_have_submission": true,
      "created_at": "2019-08-03T18:33:37.832038+07:00",
      "updated_at": "2019-08-03T18:33:54.625908+07:00"
  }
  ```

- Daftar submission dari suatu task
  > GET /situs/tasks/{id-task}/submissions

  Menampilkan seluruh submission untuk suatu task, berupa JSON sebagai berikut :
  ```json
  [
    {
        "id": 1,
        "user": {
            "id": 12,
            "username": "pande.ketut71"
        },
        "task": {
            "id": 2,
            "title": "Task : Menghibur diri sendiri",
            "start_date": "2019-08-07T22:44:52.466752+07:00",
            "end_date": "2019-08-20T18:33:32+07:00",
            "description": "Menghibur diri sendiri di rumah masing-masing. Sertakan video dirimu yang telah senang setelah menghibur diri sendiri",
            "attachment_link": "https://ga-usah-bucin-mending-menghibur-diri.com",
            "can_have_submission": true,
            "created_at": "2019-08-03T18:33:37.832038+07:00",
            "updated_at": "2019-08-03T18:33:54.625908+07:00"
            /* Notice that we are not including is_submitted field here*/
        },
        "file_link": "http://www.akusenang.com",
        "submit_time": "2019-08-19T18:33:32+07:00",
        "status": "ACCEPTED",
        "created_at": "2019-08-10T11:52:58.168026+07:00",
        "updated_at": "2019-08-10T11:53:04.934931+07:00"
    },
    {
        "id": 2,
        "user": {
            "id": 12,
            "username": "pande.ketut71"
        },
        "task": {
            "id": 2,
            "title": "Task : Menghibur diri sendiri",
            "start_date": "2019-08-07T22:44:52.466752+07:00",
            "end_date": "2019-08-20T18:33:32+07:00",
            "description": "Menghibur diri sendiri di rumah masing-masing. Sertakan video dirimu yang telah senang setelah menghibur diri sendiri",
            "attachment_link": "https://ga-usah-bucin-mending-menghibur-diri.com",
            "can_have_submission": true,
            "created_at": "2019-08-03T18:33:37.832038+07:00",
            "updated_at": "2019-08-03T18:33:54.625908+07:00"
            /* Notice that we are not including is_submitted field here*/
        },
        "file_link": "https://www.tapiboong.com",
        "submit_time": "2019-08-19T18:33:32+07:00",
        "status": "PENDING",
        "created_at": "2019-08-10T13:17:29.820313+07:00",
        "updated_at": "2019-08-10T13:18:34.518298+07:00"
    }
  ]
  ```

  Untuk menampilkan hanya submission dari user yang sedang login, tambahkan ```?user=me``` di akhir url, e.g. : ```/situs/tasks/2/submissions?user=me```

- Menambah submission untuk suatu task
  > POST /situs/tasks/{id-task}/submissions

  Menambahkan submission untuk suatu task (sesuai id yang di URL) yang diasosiasikan dengan user yang sedang login. Request body berupa JSON seperti berikut :
  ```json
  {
    "file_link": "https://www.inisubmission.com"
  }
  ```

  Hanya berupa link. Bila submission untuk task berupa file, contoh upload CV pribadi, maka lakukan upload file terlebih dahulu ke server (see [here](#File-Upload)), lalu masukkan link yang diberikan oleh server ke request body seperti diatas.

  Contoh :
  - Melakukan POST ke **/situs/tasks/2/submissions** dengan request body :
  ```json
  {
    "file_link": "https://cahyanugraha.com",
    "submit_time": "2019-08-19T18:33:32+07:00"
  }
  ```
  - Mendapatkan response berupa :
  ```json
  {
    "id": 4,
    "user": {
        "id": 12,
        "username": "pande.ketut71"
    },
    "task": {
            "id": 2,
            "title": "Task : Menghibur diri sendiri",
            "start_date": "2019-08-07T22:44:52.466752+07:00",
            "end_date": "2019-08-20T18:33:32+07:00",
            "description": "Menghibur diri sendiri di rumah masing-masing. Sertakan video dirimu yang telah senang setelah menghibur diri sendiri",
            "attachment_link": "https://ga-usah-bucin-mending-menghibur-diri.com",
            "can_have_submission": true,
            "created_at": "2019-08-03T18:33:37.832038+07:00",
            "updated_at": "2019-08-03T18:33:54.625908+07:00"
            /* Notice that we are not including is_submitted field here*/
    },
    "file_link": "https://cahyanugraha.com",
    "submit_time": "2019-08-19T18:33:32+07:00",
    "status": "PENDING",
    "created_at": "2019-08-10T14:12:25.020775+07:00",
    "updated_at": "2019-08-10T14:12:25.020775+07:00"
  }
  ```

- Daftar status submission
  - PENDING
  - ACCEPTED
  - REJECTED

#### QnA
- Get QnA List
  
  Tidak ada endpoint khusus. Detail dari announcement, task, dan event sudah termasuk daftar qna didalam JSONnya.

- Create a QnA
  > POST /situs/qnas

  Buatlah POST request dengan format sebagai berikut :
  ```json
  {
    "post_type": "event", /* Valid values => announcement, task, event */
    "post_id": 1, /* Id dari announcement, task, atau event yg diinginkan */
    "comment": "appansih"
  }
  ```
  Akan mengembalikan response sebagai berikut. QnA sudah pasti ditambahkan ke detail dari post.
  ```json
  {
    "user": {
        "id": 12,
        "username": "pande.ketut71"
    },
    "post_type": {
        "id": 18,
        "app_label": "situs",
        "model": "event"
    },
    "post_object": {
        "id": 1,
        "title": "Ahahe",
        "date": "2019-08-07T23:51:37+07:00",
        "place": "ddd",
        "description": "",
        "attachment_link": null,
        "cover_image_link": null,
        "organizer": "aku",
        "organizer_image_link": null,
        "created_at": "2019-08-07T22:56:05.185248+07:00",
        "updated_at": "2019-08-10T23:51:39.187551+07:00"
    },
    "comment": "appansih",
    "created_at": "2019-08-21T22:56:07.634242+07:00",
    "updated_at": "2019-08-21T22:56:07.634242+07:00"
  } 
  ```

#### Feedback
- Mendapatkan list of feedback
  > GET /situs/feedbacks

  Akan mengembalikan response berupa daftar feedback bagi maba yang sedang login.
  ```json
  [
    {
        "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
        "content": "Rebahan",
        "image_link": null,
        "initial": "C",
        "created_at": "2019-08-24T21:01:20.090326+07:00",
        "updated_at": "2019-08-24T21:01:20.090326+07:00"
    },
    {
        "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
        "content": "Every Weekend",
        "image_link": "http://localhost:8000/media/task_2_qjZxy1B.png",
        "initial": "C",
        "created_at": "2019-08-24T21:00:27.483621+07:00",
        "updated_at": "2019-08-24T21:00:27.483621+07:00"
    }
  ]
  ```

#### Server Time
- Mendapatkan Server Time
  > GET situs/server-time

  Menampilkan server-time dengan response berupa :
  ```json
  {
    "server_time": "2019-08-10T17:49:30.580212"
  }
  ```
#### Kenalan
- Get Token
  > GET teman/token
  
  Generate token untuk elemen yang log in. Spesifikasikan batasan jumlah maba yang bisa menggunakan token dengan menambahkan request parameter `count`.  Parameter count harus berada dalam range 1 - 15
  
  Contoh : `teman/get-token?count=10`
  
  Akan mengembalikan response berupa :
  ```json
  {
    "token": "330607",
    "user": {
      "id": 4,
      "username": "ari.angga"
    },
    "count": 10,
    "start_time": "2019-08-27T21:47:10.584828+07:00",
    "end_time": "2019-08-27T21:06:12.367007+07:00",
    "created_at": "2019-08-27T21:47:10.584828+07:00",
    "updated_at": "2019-08-27T21:47:10.584828+07:00"
  }
  ```

  Bila tidak menggunakan request param `count`, maka akan mengembalikan response berupa token seperti diatas bila masih ada token yg belum expired atau habis, dan mengembalikan response `404` bila seluruh token milik user expired/used.

  Bila menggunakan request param `count`, maka akan mengembalikan response berupa token seperti diatas bila masih ada token yg belum expired atau habis, dan mengembalikan response `201` berupa token baru dengan count sesuai yang diberikan oleh request param.

- Get Kenalan List
  > GET teman/kenalan

  Mendapatkan daftar kenalan yang terkait dengan user yang sedang login. Contoh response seperti dibawah namun berbentuk list.

- Create Kenalan
  > POST teman/kenalan

  Membuat kenalan antara maba yg melakukan POST dengan user elemen dari token yang diberikan maba. Contoh request body :
  ```json
  {
    "token": "330607"
  }
  ```

  Akan mengembalikan response body sebagai berikut :
  ```json
  {
    "id": 2,
    "user_maba": {
      "id": 1,
      "profil": {
        "id": 4,
        "user": {
          "id": 12,
          "username": "pande.ketut71"
        },
        "angkatan": {
          "id": 1,
          "tahun": "2017",
          "nama": "Tarung"
        },
        "nama_lengkap": "Pande Ketut Cahya Nugraha",
        "npm": "1706028663",
        "about": "http://localhost:8000/media/1485360295.minhpupu_183.jpg",
        "jurusan": null,
        "foto": null,
        "email": "pande.ketut71@ui.ac.id",
        "no_telepon": "123",
        "tempat_lahir": "123",
        "tanggal_lahir": "2019-08-03",
        "asal_sekolah": "SMAN 3 Denpasar",
        "line_id": "123",
        "linkedin": "123",
        "instagram_id": "123",
        "github_id": "123",
        "other_website": "123",
        "created_at": "2019-07-13T20:38:44.599590+07:00",
        "updated_at": "2019-08-10T13:25:10.830477+07:00"
      },
      "kelompok": {
        "id": 1,
        "kelompok_besar": {
          "id": 1,
          "nama": "Calypso"
        },
        "kelompok_kecil": {
          "id": 1,
          "nama": "TypeScript"
        }
      },
      "created_at": "2019-08-24T20:59:30.420741+07:00",
      "updated_at": "2019-08-24T20:59:30.420741+07:00"
    },
    "user_elemen": {
      "id": 2,
      "profil": {
        "id": 4,
        "user": {
          "id": 12,
          "username": "pande.ketut71"
        },
        "angkatan": {
          "id": 1,
          "tahun": "2017",
          "nama": "Tarung"
        },
        "nama_lengkap": "Pande Ketut Cahya Nugraha",
        "npm": "1706028663",
        "about": "http://localhost:8000/media/1485360295.minhpupu_183.jpg",
        "jurusan": null,
        "foto": null,
        "email": "pande.ketut71@ui.ac.id",
        "no_telepon": "123",
        "tempat_lahir": "123",
        "tanggal_lahir": "2019-08-03",
        "asal_sekolah": "SMAN 3 Denpasar",
        "line_id": "123",
        "linkedin": "123",
        "instagram_id": "123",
        "github_id": "123",
        "other_website": "123",
        "created_at": "2019-07-13T20:38:44.599590+07:00",
        "updated_at": "2019-08-10T13:25:10.830477+07:00"
      },
      "title": null,
      "created_at": "2019-08-29T05:11:50.873548+07:00",
      "updated_at": "2019-08-29T05:11:50.873548+07:00"
    },
    "description": "",
    "rejection_message": "",
    "status": "SAVED",
    "created_at": "2019-08-29T05:14:21.934748+07:00",
    "updated_at": "2019-08-29T05:14:21.934748+07:00"
  }
  ```

- Get Kenalan
  > GET teman/kenalan/{id-kenalan}

  Mendapatkan kenalan sesuai idnya. Akan mengembalikan response body seperti diatas bila merupakan bagian dari kenalan, selain itu mendapatkan response 403 Forbidden.

- Edit Kenalan
  > PATCH teman/kenalan/{id-kenalan}

  Untuk Maba :
  - Maba hanya bisa mengedit kenalan yang statusnya Not Friend, Saved, atau Rejected. Field yang bisa diedit hanyalah description dan status.
  - Contoh request body :
    ```json
    {
      "description": "Kenalan yuk",
      "status": "SAVED"
    }
    ```

    ```json
    {
      "description": "Kenalan yuk",
      "status": "PENDING"
    }
    ```

    Perhatikan bahwa kita hanya bisa mengubah status menjadi SAVED atau PENDING. Lalu, mengubah ke status SAVED hanya bisa dilakukan bila kurang dari 10 kenalan milik maba mempunyai status SAVED
  
  Untuk Elemen :
  - Elemen bisa menerima atau menolak suatu kenalan. Elemen hanya bisa melakukan kedua hal itu bila status kenalannya Pending atau Rejected.
  - Contoh request body :
    ```json
    {
      "is_approved": false,
      "rejection_message": "Kamu kurang ganteng cah"
    }
    ```
    ```json
    {
      "is_approved": true
    }
    ```

- Set Kenalan to Not Friend
  > DELETE /teman/kenalan/{id-kenalan}

  Melakukan reset terhadap objek kenalan menjadi seperti state awal saat memasukkan token. Return Response `204`.

- Daftar status kenalan
  - NOT_FRIEND
  - PENDING
  - ACCEPTED
  - REJECTED
  - SAVED

- Get Kenalan Task
  > GET /teman/kenalan-task

    Mengembalikan semua task kenalan dengan response body sebagai berikut:
    ```json
    [
      {
        "id": 6,
        "title": "Kenalan",
        "start_date": "2019-09-04T21:31:29+07:00",
        "end_date": "2019-09-06T21:31:31+07:00",
        "required_quanta": 1,
        "required_tarung": 1,
        "required_omega": 1,
        "required_elemen": 1,
        "required_all": 1,
        "created_at": "2019-09-05T22:02:05.246347+07:00",
        "updated_at": "2019-09-05T22:08:17.452828+07:00"
      }
    ]
    ```

- Get Kenalan User Statistics
  > GET /teman/kenalan-user-statistics

    Mengembalikan semua statisitik kenalan seorang maba dengan response body sebagai berikut:
    ```json
    [
      {
        "id": 1,
        "user": {
          "id": 15,
          "username": "pande.ketut71"
        },
        "kenalan_task": {
          "id": 6,
          "title": "Kenalan",
          "start_date": "2019-09-04T21:31:29+07:00",
          "end_date": "2019-09-06T21:31:31+07:00",
          "required_quanta": 1,
          "required_tarung": 1,
          "required_omega": 1,
          "required_elemen": 1,
          "required_all": 1,
          "created_at": "2019-09-05T22:02:05.246347+07:00",
          "updated_at": "2019-09-05T22:08:17.452828+07:00"
        },
        "kenalan_created_quanta": 0,
        "kenalan_created_tarung": 1,
        "kenalan_created_omega": 0,
        "kenalan_created_elemen": 0,
        "kenalan_created_all": 0,
        "kenalan_approved_quanta": 0,
        "kenalan_approved_tarung": 1,
        "kenalan_approved_omega": 0,
        "kenalan_approved_elemen": 0,
        "kenalan_approved_all": 0
      }
    ]
    ```

#### Interest
- Get Interest List
  > GET /akun/interests

    Mendapatkan daftar seluruh interest. By default akan mengembalikan daftar interest semua user web.
    Contoh response body:
    ```json
    [
      {
        "id": 1,
        "profil": {
          "id": 7,
          "user": {
            "id": 2,
            "username": "anthrocoon12"
          },
          "angkatan": {
            "id": 1,
            "tahun": "2017",
            "nama": "Tarung"
          },
          "nama_lengkap": "ANTHROCOON",
          "about": "",
          "jurusan": null,
          "foto": null,
          "email": null,
          "created_at": "2019-08-29T22:27:52.218407+07:00",
          "updated_at": "2019-08-29T22:27:52.218407+07:00"
        },
        "interest_name": "Coding",
        "interest_category": "Technology",
        "is_top_interest": false,
        "created_at": "2019-08-31T21:15:23.927254+07:00",
        "updated_at": "2019-08-31T21:15:23.927254+07:00"
      },
      {
        "id": 8,
        "profil": {
          "id": 6,
          "user": {
            "id": 15,
            "username": "pande.ketut71"
          },
          "angkatan": {
            "id": 1,
            "tahun": "2017",
            "nama": "Tarung"
          },
          "nama_lengkap": "Pande Ketut Cahya Nugraha",
          "about": null,
          "jurusan": null,
          "foto": null,
          "created_at": "2019-08-29T22:26:25.796390+07:00",
          "updated_at": "2019-08-29T22:26:25.796390+07:00"
        },
        "interest_name": "Tidur",
        "interest_category": "Non-Tech",
        "is_top_interest": false,
        "created_at": "2019-08-31T21:25:53.429459+07:00",
        "updated_at": "2019-08-31T21:25:53.429459+07:00"
      }
    ]
    ```

    Untuk mendapatkan daftar interest seorang user, bisa dilakukan dengan menambahkan query string `user`.
    > /akun/interests?user=15
    
    Contoh response body :
    ```json
    [
      {
        "id": 8,
        "profil": {
          "id": 6,
          "user": {
            "id": 15,
            "username": "pande.ketut71"
          },
          "angkatan": {
            "id": 1,
            "tahun": "2017",
            "nama": "Tarung"
          },
          "nama_lengkap": "Pande Ketut Cahya Nugraha",
          "about": null,
          "jurusan": null,
          "foto": null,
          "created_at": "2019-08-29T22:26:25.796390+07:00",
          "updated_at": "2019-08-29T22:26:25.796390+07:00"
        },
        "interest_name": "ASDZfxg",
        "interest_category": "asDzfgh",
        "is_top_interest": false,
        "created_at": "2019-08-31T21:25:53.429459+07:00",
        "updated_at": "2019-08-31T21:25:53.429459+07:00"
      },
      {
        "id": 9,
        "profil": {
          "id": 6,
          "user": {
            "id": 15,
            "username": "pande.ketut71"
          },
          "angkatan": {
            "id": 1,
            "tahun": "2017",
            "nama": "Tarung"
          },
          "nama_lengkap": "Pande Ketut Cahya Nugraha",
          "about": null,
          "jurusan": null,
          "foto": null,
          "created_at": "2019-08-29T22:26:25.796390+07:00",
          "updated_at": "2019-08-29T22:26:25.796390+07:00"
        },
        "interest_name": "Nembak",
        "interest_category": "Non-Tech",
        "is_top_interest": false,
        "created_at": "2019-08-31T21:25:53.429459+07:00",
        "updated_at": "2019-08-31T21:25:53.429459+07:00"
      }
    ]
    ```

- Add Interest
  > POST /akun/interests

  Untuk menambahkan interest ke user yang sedang login, lakukan POST request dengan request body :
  ```json
  {
    "interest_name": "Musik",
    "interest_category": "Entertainment"
  }
  ```

  Akan mengembalikan response body berupa sebuah object Interest.
  ```json
  {
    "id": 9,
    "profil": {
      "id": 6,
      "user": {
        "id": 15,
        "username": "pande.ketut71"
      },
      "angkatan": {
        "id": 1,
        "tahun": "2017",
        "nama": "Tarung"
      },
      "nama_lengkap": "Pande Ketut Cahya Nugraha",
      "about": null,
      "jurusan": null,
      "foto": null,
      "created_at": "2019-08-29T22:26:25.796390+07:00",
      "updated_at": "2019-08-29T22:26:25.796390+07:00"
    },
    "interest_name": "Musik",
    "interest_category": "Entertainment",
    "is_top_interest": false,
    "created_at": "2019-08-31T21:25:53.429459+07:00",
    "updated_at": "2019-08-31T21:25:53.429459+07:00"
  }
  ```

- Delete an Interest
  > DELETE /akun/interests/{id-interest}

  Kita bisa menghapus salah satu interest dari user yang sedang login. Lakukan DELETE request ke `/akun/interests/{id-interest}`. Contoh :
  
  Dengan asumsi sedang login ke user pande.ketut71
  > DELETE /akun/interests/9

  Maka interest milik pande.ketut71 pada contoh `Add Interest` akan dihapus.

  Tidak dapat menghapus interest milik orang lain.

- Menandai suatu interest merupakan top interest di kategori tersebut
  > PATCH /akun/interests/{id-interest}

  Akan membuat semua interest dari user yg sedang login pada satu kategori dengan interest yg ditunjuk oleh id interest menjadi non top interest, dan interest yang ditunjuk oleh id menjadi top interest

  Contoh :
  > Sebelum diubah
  ```json
  [
    {
      "id": 8,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "ASDZfxg",
      "interest_category": "Entertainment",
      "is_top_interest": true,
      "created_at": "2019-08-31T21:25:53.429459+07:00",
      "updated_at": "2019-09-01T21:10:51.883824+07:00"
    },
    {
      "id": 9,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "Ketut",
      "interest_category": "Entertainment",
      "is_top_interest": false,
      "created_at": "2019-09-01T20:48:15.799382+07:00",
      "updated_at": "2019-09-01T21:10:25.262132+07:00"
    },
    {
      "id": 12,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "Nugraha",
      "interest_category": "Non-Tech",
      "is_top_interest": true,
      "created_at": "2019-09-01T20:58:49.031900+07:00",
      "updated_at": "2019-09-01T21:10:13.105845+07:00"
    },
    {
      "id": 13,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "Jumping",
      "interest_category": "Non-Tech",
      "is_top_interest": false,
      "created_at": "2019-09-01T20:58:49.031900+07:00",
      "updated_at": "2019-09-01T21:10:13.105845+07:00"
    }
  ]
  ```
  > Setelah memanggil PATCH /akun/interests/9
  ```json
  [
    {
      "id": 9,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "Ketut",
      "interest_category": "Entertainment",
      "is_top_interest": true,
      "created_at": "2019-09-01T20:48:15.799382+07:00",
      "updated_at": "2019-09-01T21:10:25.262132+07:00"
    },
    {
      "id": 8,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "ASDZfxg",
      "interest_category": "Entertainment",
      "is_top_interest": false,
      "created_at": "2019-08-31T21:25:53.429459+07:00",
      "updated_at": "2019-09-01T21:10:51.883824+07:00"
    },
    {
      "id": 12,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "Nugraha",
      "interest_category": "Non-Tech",
      "is_top_interest": true,
      "created_at": "2019-09-01T20:58:49.031900+07:00",
      "updated_at": "2019-09-01T21:10:13.105845+07:00"
    },
    {
      "id": 13,
      "profil": {
        /* Omitted for simplicity */
      },
      "interest_name": "Jumping",
      "interest_category": "Non-Tech",
      "is_top_interest": false,
      "created_at": "2019-09-01T20:58:49.031900+07:00",
      "updated_at": "2019-09-01T21:10:13.105845+07:00"
    }
  ]
  ```

  Response body adalah semua interest milik user yang sedang login.

  Tidak dapat mengubah interest milik orang lain.

- Daftar Category Interest
  - Technology
  - Non-Tech
  - Entertainment

#### Story
- Get stories
  > GET /situs/stories

  Kalo ingin dapet yg dipunya sama user yg lagi login doang
  > GET /situs/stories?user=me

  Hasil : 
  ```json
  [
        {
            "id": 2,
            "user": {
                "id": 15,
                "username": "pande.ketut71",
                "user_detail": {
                    "id": 4,
                    "profil": {
                        "id": 6,
                        "user": {
                            "id": 15,
                            "username": "pande.ketut71"
                        },
                        "angkatan": {
                            "id": 1,
                            "tahun": "2017",
                            "nama": "Tarung"
                        },
                        "nama_lengkap": "Pande Ketut Cahya Nugraha",
                        "npm": "1706028663",
                        "about": null,
                        "jurusan": null,
                        "foto": null,
                        "email": "pande.ketut71@ui.ac.id",
                        "no_telepon": null,
                        "tempat_lahir": null,
                        "tanggal_lahir": null,
                        "asal_sekolah": null,
                        "line_id": null,
                        "linkedin": null,
                        "instagram_id": null,
                        "github_id": null,
                        "other_website": null,
                        "created_at": "2019-08-29T22:26:25.796390+07:00",
                        "updated_at": "2019-08-29T22:26:25.796390+07:00"
                    },
                    "kelompok": {
                        "id": 1,
                        "kelompok_besar": {
                            "id": 1,
                            "nama": "Calypso"
                        },
                        "kelompok_kecil": {
                            "id": 1,
                            "nama": "TypeScript"
                        }
                    },
                    "pengoreksi": "OWH",
                    "created_at": "2019-08-29T22:27:30.290339+07:00",
                    "updated_at": "2019-09-13T20:52:38.497302+07:00"
                }
            },
            "medium": {
                "title": "How to manage concurrency in Django models",
                "latest_published_at": 1545589180215,
                "subtitle": "For a better reading experience, check out this article on my website.",
                "thumbnail": {
                    "name": "1b22",
                    "type": 4,
                    "text": "Photo by Denys Nevozhai",
                    "markups": [
                        {
                            "type": 3,
                            "start": 8,
                            "end": 23,
                            "href": "http://unsplash.com/photos/7nrsVjvALnA?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText",
                            "title": "",
                            "rel": "noopener",
                            "anchorType": 0
                        }
                    ],
                    "layout": 3,
                    "metadata": {
                        "id": "0*3zQWA1e5JsspQgQY.",
                        "originalWidth": 1400,
                        "originalHeight": 1049,
                        "isFeatured": true
                    }
                }
            },
            "link": "https://medium.com/@hakibenita/how-to-manage-concurrency-in-django-models-b240fed4ee2",
            "created_at": "2019-09-27T22:32:54.515087+07:00",
            "updated_at": "2019-09-27T22:32:54.515135+07:00"
        },
        {
            "id": 1,
            "user": {
                "id": 2,
                "username": "anthrocoon12",
                "user_detail": {
                    "id": 4,
                    "profil": {
                        "id": 7,
                        "user": {
                            "id": 2,
                            "username": "anthrocoon12"
                        },
                        "angkatan": {
                            "id": 1,
                            "tahun": "2017",
                            "nama": "Tarung"
                        },
                        "nama_lengkap": "ANTHROCOON",
                        "npm": "123456789",
                        "about": "",
                        "jurusan": null,
                        "foto": null,
                        "email": null,
                        "no_telepon": null,
                        "tempat_lahir": null,
                        "tanggal_lahir": null,
                        "asal_sekolah": null,
                        "line_id": null,
                        "linkedin": null,
                        "instagram_id": null,
                        "github_id": null,
                        "other_website": null,
                        "created_at": "2019-08-29T22:27:52.218407+07:00",
                        "updated_at": "2019-08-29T22:27:52.218407+07:00"
                    },
                    "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
                    "created_at": "2019-08-29T22:28:04.508548+07:00",
                    "updated_at": "2019-08-29T22:28:04.508548+07:00"
                }
            },
            "medium": {
                "title": "How to manage concurrency in Django models",
                "latest_published_at": 1545589180215,
                "subtitle": "For a better reading experience, check out this article on my website.",
                "thumbnail": {
                    "name": "1b22",
                    "type": 4,
                    "text": "Photo by Denys Nevozhai",
                    "markups": [
                        {
                            "type": 3,
                            "start": 8,
                            "end": 23,
                            "href": "http://unsplash.com/photos/7nrsVjvALnA?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText",
                            "title": "",
                            "rel": "noopener",
                            "anchorType": 0
                        }
                    ],
                    "layout": 3,
                    "metadata": {
                        "id": "0*3zQWA1e5JsspQgQY.",
                        "originalWidth": 1400,
                        "originalHeight": 1049,
                        "isFeatured": true
                    }
                }
            },
            "link": "https://medium.com/@hakibenita/how-to-manage-concurrency-in-django-models-b240fed4ee2",
            "created_at": "2019-09-27T22:22:13.201221+07:00",
            "updated_at": "2019-09-27T22:22:13.201266+07:00"
        }
    ]
  ```

- Create story
  > POST /situs/stories
    Request body :
    ```json
    {
      "link": "https://medium.com/@hakibenita/how-to-manage-concurrency-in-django-models-b240fed4ee2"
    }
    ```

    Kalo request body salah, return status code 400 Bad Request, kalo link medium salah, status code 406 NOT ACCEPTABLE

    Response kalo 201 CREATED:
    ```json
    {
        "id": 2,
        "user": {
            "id": 15,
            "username": "pande.ketut71",
            "user_detail": {
                "id": 4,
                "profil": {
                    "id": 6,
                    "user": {
                        "id": 15,
                        "username": "pande.ketut71"
                    },
                    "angkatan": {
                        "id": 1,
                        "tahun": "2017",
                        "nama": "Tarung"
                    },
                    "nama_lengkap": "Pande Ketut Cahya Nugraha",
                    "npm": "1706028663",
                    "about": null,
                    "jurusan": null,
                    "foto": null,
                    "email": "pande.ketut71@ui.ac.id",
                    "no_telepon": null,
                    "tempat_lahir": null,
                    "tanggal_lahir": null,
                    "asal_sekolah": null,
                    "line_id": null,
                    "linkedin": null,
                    "instagram_id": null,
                    "github_id": null,
                    "other_website": null,
                    "created_at": "2019-08-29T22:26:25.796390+07:00",
                    "updated_at": "2019-08-29T22:26:25.796390+07:00"
                },
                "kelompok": {
                    "id": 1,
                    "kelompok_besar": {
                        "id": 1,
                        "nama": "Calypso"
                    },
                    "kelompok_kecil": {
                        "id": 1,
                        "nama": "TypeScript"
                    }
                },
                "pengoreksi": "OWH",
                "created_at": "2019-08-29T22:27:30.290339+07:00",
                "updated_at": "2019-09-13T20:52:38.497302+07:00"
            }
        },
        "medium": {
            "title": "How to manage concurrency in Django models",
            "latest_published_at": 1545589180215,
            "subtitle": "For a better reading experience, check out this article on my website.",
            "thumbnail": {
                "name": "1b22",
                "type": 4,
                "text": "Photo by Denys Nevozhai",
                "markups": [
                    {
                        "type": 3,
                        "start": 8,
                        "end": 23,
                        "href": "http://unsplash.com/photos/7nrsVjvALnA?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText",
                        "title": "",
                        "rel": "noopener",
                        "anchorType": 0
                    }
                ],
                "layout": 3,
                "metadata": {
                    "id": "0*3zQWA1e5JsspQgQY.",
                    "originalWidth": 1400,
                    "originalHeight": 1049,
                    "isFeatured": true
                }
            }
        },
        "link": "https://medium.com/@hakibenita/how-to-manage-concurrency-in-django-models-b240fed4ee2",
        "created_at": "2019-09-27T22:32:54.515087+07:00",
        "updated_at": "2019-09-27T22:32:54.515135+07:00"
    }
    ```

- Edit story
  > PATCH /situs/stories/{id-story}

  Literally sama kayak create story.

- Delete story
  > DELETE /situs/stories/{id-story}

  Hanya owner dari story yang dapat menghapus suatu story. 
  Return 200 OK bila berhasil, 403 Forbidden bila yg mencoba menghapus story bukan yg memiliki story tersebut.

#### Notification
- Get notification
  > GET /situs/notifications

    Dapetin semua notif dari user yg login

    Cth (Lagi keburu ngejar deadline hehe, ntar direfinenya, bilang aja field mana yg dipake mana yg ngga):
    ```json
    [
      {
          "id": 15,
          "related_object_type": {
              "id": 27,
              "app_label": "teman",
              "model": "kenalan"
          },
          "related_object": {
              "id": 6,
              "user_maba": {
                  "id": 4,
                  "profil": {
                      "id": 6,
                      "user": {
                          "id": 15,
                          "username": "pande.ketut71"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "Pande Ketut Cahya Nugraha",
                      "npm": "1706028663",
                      "about": null,
                      "jurusan": null,
                      "foto": null,
                      "email": "pande.ketut71@ui.ac.id",
                      "no_telepon": null,
                      "tempat_lahir": null,
                      "tanggal_lahir": null,
                      "asal_sekolah": null,
                      "line_id": null,
                      "linkedin": null,
                      "instagram_id": null,
                      "github_id": null,
                      "other_website": null,
                      "created_at": "2019-08-29T22:26:25.796390+07:00",
                      "updated_at": "2019-08-29T22:26:25.796390+07:00"
                  },
                  "kelompok": {
                      "id": 1,
                      "kelompok_besar": {
                          "id": 1,
                          "nama": "Calypso"
                      },
                      "kelompok_kecil": {
                          "id": 1,
                          "nama": "TypeScript"
                      }
                  },
                  "pengoreksi": "OWH",
                  "created_at": "2019-08-29T22:27:30.290339+07:00",
                  "updated_at": "2019-09-13T20:52:38.497302+07:00"
              },
              "user_elemen": {
                  "id": 4,
                  "profil": {
                      "id": 7,
                      "user": {
                          "id": 2,
                          "username": "anthrocoon12"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "ANTHROCOON",
                      "npm": "123456789",
                      "about": "",
                      "jurusan": null,
                      "foto": null,
                      "email": null,
                      "no_telepon": null,
                      "tempat_lahir": null,
                      "tanggal_lahir": null,
                      "asal_sekolah": null,
                      "line_id": null,
                      "linkedin": null,
                      "instagram_id": null,
                      "github_id": null,
                      "other_website": null,
                      "created_at": "2019-08-29T22:27:52.218407+07:00",
                      "updated_at": "2019-08-29T22:27:52.218407+07:00"
                  },
                  "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
                  "created_at": "2019-08-29T22:28:04.508548+07:00",
                  "updated_at": "2019-08-29T22:28:04.508548+07:00"
              },
              "description": "PMB",
              "rejection_message": "",
              "status": "ACCEPTED",
              "created_at": "2019-09-16T21:32:23.587215+07:00",
              "updated_at": "2019-09-27T22:24:20.620791+07:00"
          },
          "notification_type": "KENALAN_ACCEPTED",
          "related_object_id": 6,
          "is_viewed": false,
          "created_at": "2019-09-27T22:24:20.635967+07:00",
          "updated_at": "2019-09-27T22:24:20.636007+07:00",
          "user": 15
      },
      {
          "id": 14,
          "related_object_type": {
              "id": 27,
              "app_label": "teman",
              "model": "kenalan"
          },
          "related_object": {
              "id": 6,
              "user_maba": {
                  "id": 4,
                  "profil": {
                      "id": 6,
                      "user": {
                          "id": 15,
                          "username": "pande.ketut71"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "Pande Ketut Cahya Nugraha",
                      "npm": "1706028663",
                      "about": null,
                      "jurusan": null,
                      "foto": null,
                      "email": "pande.ketut71@ui.ac.id",
                      "no_telepon": null,
                      "tempat_lahir": null,
                      "tanggal_lahir": null,
                      "asal_sekolah": null,
                      "line_id": null,
                      "linkedin": null,
                      "instagram_id": null,
                      "github_id": null,
                      "other_website": null,
                      "created_at": "2019-08-29T22:26:25.796390+07:00",
                      "updated_at": "2019-08-29T22:26:25.796390+07:00"
                  },
                  "kelompok": {
                      "id": 1,
                      "kelompok_besar": {
                          "id": 1,
                          "nama": "Calypso"
                      },
                      "kelompok_kecil": {
                          "id": 1,
                          "nama": "TypeScript"
                      }
                  },
                  "pengoreksi": "OWH",
                  "created_at": "2019-08-29T22:27:30.290339+07:00",
                  "updated_at": "2019-09-13T20:52:38.497302+07:00"
              },
              "user_elemen": {
                  "id": 4,
                  "profil": {
                      "id": 7,
                      "user": {
                          "id": 2,
                          "username": "anthrocoon12"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "ANTHROCOON",
                      "npm": "123456789",
                      "about": "",
                      "jurusan": null,
                      "foto": null,
                      "email": null,
                      "no_telepon": null,
                      "tempat_lahir": null,
                      "tanggal_lahir": null,
                      "asal_sekolah": null,
                      "line_id": null,
                      "linkedin": null,
                      "instagram_id": null,
                      "github_id": null,
                      "other_website": null,
                      "created_at": "2019-08-29T22:27:52.218407+07:00",
                      "updated_at": "2019-08-29T22:27:52.218407+07:00"
                  },
                  "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
                  "created_at": "2019-08-29T22:28:04.508548+07:00",
                  "updated_at": "2019-08-29T22:28:04.508548+07:00"
              },
              "description": "PMB",
              "rejection_message": "",
              "status": "ACCEPTED",
              "created_at": "2019-09-16T21:32:23.587215+07:00",
              "updated_at": "2019-09-27T22:24:20.620791+07:00"
          },
          "notification_type": "KENALAN_REJECTED",
          "related_object_id": 6,
          "is_viewed": false,
          "created_at": "2019-09-27T22:24:16.370531+07:00",
          "updated_at": "2019-09-27T22:24:16.370669+07:00",
          "user": 15
      },
      {
          "id": 13,
          "related_object_type": {
              "id": 27,
              "app_label": "teman",
              "model": "kenalan"
          },
          "related_object": {
              "id": 6,
              "user_maba": {
                  "id": 4,
                  "profil": {
                      "id": 6,
                      "user": {
                          "id": 15,
                          "username": "pande.ketut71"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "Pande Ketut Cahya Nugraha",
                      "npm": "1706028663",
                      "about": null,
                      "jurusan": null,
                      "foto": null,
                      "email": "pande.ketut71@ui.ac.id",
                      "no_telepon": null,
                      "tempat_lahir": null,
                      "tanggal_lahir": null,
                      "asal_sekolah": null,
                      "line_id": null,
                      "linkedin": null,
                      "instagram_id": null,
                      "github_id": null,
                      "other_website": null,
                      "created_at": "2019-08-29T22:26:25.796390+07:00",
                      "updated_at": "2019-08-29T22:26:25.796390+07:00"
                  },
                  "kelompok": {
                      "id": 1,
                      "kelompok_besar": {
                          "id": 1,
                          "nama": "Calypso"
                      },
                      "kelompok_kecil": {
                          "id": 1,
                          "nama": "TypeScript"
                      }
                  },
                  "pengoreksi": "OWH",
                  "created_at": "2019-08-29T22:27:30.290339+07:00",
                  "updated_at": "2019-09-13T20:52:38.497302+07:00"
              },
              "user_elemen": {
                  "id": 4,
                  "profil": {
                      "id": 7,
                      "user": {
                          "id": 2,
                          "username": "anthrocoon12"
                      },
                      "angkatan": {
                          "id": 1,
                          "tahun": "2017",
                          "nama": "Tarung"
                      },
                      "nama_lengkap": "ANTHROCOON",
                      "npm": "123456789",
                      "about": "",
                      "jurusan": null,
                      "foto": null,
                      "email": null,
                      "no_telepon": null,
                      "tempat_lahir": null,
                      "tanggal_lahir": null,
                      "asal_sekolah": null,
                      "line_id": null,
                      "linkedin": null,
                      "instagram_id": null,
                      "github_id": null,
                      "other_website": null,
                      "created_at": "2019-08-29T22:27:52.218407+07:00",
                      "updated_at": "2019-08-29T22:27:52.218407+07:00"
                  },
                  "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
                  "created_at": "2019-08-29T22:28:04.508548+07:00",
                  "updated_at": "2019-08-29T22:28:04.508548+07:00"
              },
              "description": "PMB",
              "rejection_message": "",
              "status": "ACCEPTED",
              "created_at": "2019-09-16T21:32:23.587215+07:00",
              "updated_at": "2019-09-27T22:24:20.620791+07:00"
          },
          "notification_type": "KENALAN_PENDING",
          "related_object_id": 6,
          "is_viewed": false,
          "created_at": "2019-09-27T22:24:12.117335+07:00",
          "updated_at": "2019-09-27T22:24:12.117374+07:00",
          "user": 15
      },
      {
          "id": 11,
          "related_object_type": {
              "id": 32,
              "app_label": "situs",
              "model": "story"
          },
          "related_object": {
              "id": 1,
              "user": {
                  "id": 2,
                  "username": "anthrocoon12",
                  "user_detail": {
                      "id": 4,
                      "profil": {
                          "id": 7,
                          "user": {
                              "id": 2,
                              "username": "anthrocoon12"
                          },
                          "angkatan": {
                              "id": 1,
                              "tahun": "2017",
                              "nama": "Tarung"
                          },
                          "nama_lengkap": "ANTHROCOON",
                          "npm": "123456789",
                          "about": "",
                          "jurusan": null,
                          "foto": null,
                          "email": null,
                          "no_telepon": null,
                          "tempat_lahir": null,
                          "tanggal_lahir": null,
                          "asal_sekolah": null,
                          "line_id": null,
                          "linkedin": null,
                          "instagram_id": null,
                          "github_id": null,
                          "other_website": null,
                          "created_at": "2019-08-29T22:27:52.218407+07:00",
                          "updated_at": "2019-08-29T22:27:52.218407+07:00"
                      },
                      "title": "Mail Mencari Banyaknya Kemungkinan ABCDEF",
                      "created_at": "2019-08-29T22:28:04.508548+07:00",
                      "updated_at": "2019-08-29T22:28:04.508548+07:00"
                  }
              },
              "link": "https://medium.com/@hakibenita/how-to-manage-concurrency-in-django-models-b240fed4ee2",
              "created_at": "2019-09-27T22:22:13.201221+07:00",
              "updated_at": "2019-09-27T22:22:13.201266+07:00"
          },
          "notification_type": "NEW_STORY",
          "related_object_id": 1,
          "is_viewed": false,
          "created_at": "2019-09-27T22:22:13.220291+07:00",
          "updated_at": "2019-09-27T22:22:13.220339+07:00",
          "user": 15
      },
      {
          "id": 8,
          "related_object_type": {
              "id": 25,
              "app_label": "situs",
              "model": "feedback"
          },
          "related_object": {
              "title": "Test",
              "content": "a",
              "image_link": null,
              "initial": "aaa",
              "created_at": "2019-09-27T21:35:04.649766+07:00",
              "updated_at": "2019-09-27T21:35:04.649809+07:00"
          },
          "notification_type": "NEW_FEEDBACK",
          "related_object_id": 3,
          "is_viewed": false,
          "created_at": "2019-09-27T21:35:04.674822+07:00",
          "updated_at": "2019-09-27T21:35:04.674863+07:00",
          "user": 15
      },
      {
          "id": 7,
          "related_object_type": {
              "id": 20,
              "app_label": "situs",
              "model": "task"
          },
          "related_object": {
              "id": 4,
              "title": "Task 3",
              "start_date": "2019-09-27T21:28:09+07:00",
              "end_date": "2019-09-27T21:28:10+07:00",
              "description": "",
              "attachment_link": null,
              "can_have_submission": true,
              "created_at": "2019-09-27T21:28:11.636794+07:00",
              "updated_at": "2019-09-27T21:28:11.636826+07:00"
          },
          "notification_type": "NEW_TASK",
          "related_object_id": 4,
          "is_viewed": false,
          "created_at": "2019-09-27T21:28:11.661014+07:00",
          "updated_at": "2019-09-27T21:28:11.661041+07:00",
          "user": 15
      },
      {
          "id": 5,
          "related_object_type": {
              "id": 18,
              "app_label": "situs",
              "model": "event"
          },
          "related_object": {
              "id": 4,
              "title": "Event 3",
              "date": "2019-09-27T21:04:00+07:00",
              "place": "Balairung",
              "description": "",
              "attachment_link": null,
              "cover_image_link": null,
              "organizer": null,
              "organizer_image_link": null,
              "created_at": "2019-09-27T21:27:59.194786+07:00",
              "updated_at": "2019-09-27T21:27:59.194826+07:00"
          },
          "notification_type": "NEW_EVENT",
          "related_object_id": 4,
          "is_viewed": false,
          "created_at": "2019-09-27T21:27:59.225499+07:00",
          "updated_at": "2019-09-27T21:27:59.225549+07:00",
          "user": 15
      },
      {
          "id": 3,
          "related_object_type": {
              "id": 24,
              "app_label": "situs",
              "model": "announcement"
          },
          "related_object": {
              "id": 6,
              "title": "Announcement 4",
              "date": "2019-09-27T21:03:52+07:00",
              "description": "",
              "attachment_link": null,
              "created_at": "2019-09-27T21:27:56.114406+07:00",
              "updated_at": "2019-09-27T21:27:56.114437+07:00"
          },
          "notification_type": "NEW_ANNOUNCEMENT",
          "related_object_id": 6,
          "is_viewed": false,
          "created_at": "2019-09-27T21:27:56.138716+07:00",
          "updated_at": "2019-09-27T21:27:56.138745+07:00",
          "user": 15
      }
    ]
    ```

- Set notification as viewed
  > PATCH /situs/notifications/{id-notification>

  Bisa get individual notif pake GET juga btw.

#### Find Friends
- Get daftar orang yang interested dengan suatu interest
  > GET /teman/find-friends?interest={nama interestnya}

  Contoh:
  > teman/find-friends?interest=dev
  
  Response body:
  ```json
  [
      {
          "id": 841,
          "username": "muhammad.ryan81",
          "user_detail": {
              "id": 405,
              "profil": {
                  "id": 837,
                  "user": {
                      "id": 841,
                      "username": "muhammad.ryan81"
                  },
                  "angkatan": {
                      "id": 3,
                      "tahun": "2018",
                      "nama": "Quanta"
                  },
                  "interests": [
                      {
                          "interest_name": "Marvel",
                          "interest_category": "Entertainment",
                          "is_top_interest": true
                      },
                      {
                          "interest_name": "Photography",
                          "interest_category": "Non-Tech",
                          "is_top_interest": true
                      },
                      {
                          "interest_name": "Game Development",
                          "interest_category": "Technology",
                          "is_top_interest": true
                      }
                  ],
                  "nama_lengkap": "Muhammad Ryan Anshar Haryanto",
                  "npm": "1806147073",
                  "about": null,
                  "jurusan": null,
                  "foto": null,
                  "email": "muhammad.ryan81@ui.ac.id",
                  "no_telepon": null,
                  "tempat_lahir": "Pekalongan",
                  "tanggal_lahir": "2000-07-21",
                  "asal_sekolah": "SMAN 3 Jakarta",
                  "line_id": "ryananshar",
                  "linkedin": "",
                  "instagram_id": null,
                  "github_id": null,
                  "other_website": null,
                  "is_private": false,
                  "created_at": "2019-09-10T17:11:50.175124+07:00",
                  "updated_at": "2019-09-10T17:13:08.294375+07:00"
              },
              "title": "Fasilkom 2018",
              "created_at": "2019-09-10T17:11:50.176725+07:00",
              "updated_at": "2019-09-10T17:13:08.296022+07:00"
          }
      },
      {
          "id": 13,
          "username": "nabilah.adani",
          "user_detail": {
              "id": 12,
              "profil": {
                  "id": 12,
                  "user": {
                      "id": 13,
                      "username": "nabilah.adani"
                  },
                  "angkatan": {
                      "id": 1,
                      "tahun": "2017",
                      "nama": "Tarung"
                  },
                  "interests": [
                      {
                          "interest_name": "Anime",
                          "interest_category": "Entertainment",
                          "is_top_interest": false
                      },
                      {
                          "interest_name": "Band",
                          "interest_category": "Non-Tech",
                          "is_top_interest": true
                      },
                      {
                          "interest_name": "Data Science",
                          "interest_category": "Technology",
                          "is_top_interest": false
                      },
                      {
                          "interest_name": "Web Development",
                          "interest_category": "Technology",
                          "is_top_interest": false
                      }
                  ],
                  "nama_lengkap": "Nabilah Adani Nurulizzah",
                  "npm": "1706979386",
                  "about": null,
                  "jurusan": null,
                  "foto": "https://pmb.novi-bot.online/media/Screenshot_2018-02-11-14-35-08-69.png",
                  "email": "nabilah.adani@ui.ac.id",
                  "no_telepon": null,
                  "tempat_lahir": "Jakarta",
                  "tanggal_lahir": "1999-02-19",
                  "asal_sekolah": "SMAN 3 Depok",
                  "line_id": "17caratx",
                  "linkedin": "",
                  "instagram_id": null,
                  "github_id": null,
                  "other_website": null,
                  "is_private": false,
                  "created_at": "2019-08-11T17:30:20.656742+07:00",
                  "updated_at": "2019-09-11T10:28:48.009209+07:00"
              },
              "title": "Wakil Ketua Pelaksana PMB 2019",
              "created_at": "2019-08-11T17:30:20.658297+07:00",
              "updated_at": "2019-09-11T10:28:48.010964+07:00"
          }
      }
  ]
  ```

#### Kata Elemen
- Mendapatkan daftar kata elemen
  > GET /situs/kata-elemens

  Hasil : 
  ```json
  [
    {
        "id": 6,
        "user": {
            "id": 1,
            "profil": {
                "id": 1,
                "user": {
                    "id": 1,
                    "username": "ari.angga"
                },
                "angkatan": {
                    "id": 1,
                    "tahun": "2018",
                    "nama": "Quanta"
                },
                "interests": [
                    {
                        "interest_name": "Dota",
                        "interest_category": "Technology",
                        "is_top_interest": false
                    }
                ],
                "nama_lengkap": "Ari Angga Nugraha",
                "npm": "1806205086",
                "about": null,
                "jurusan": null,
                "foto": null,
                "email": "ari.angga@ui.ac.id",
                "no_telepon": null,
                "tempat_lahir": null,
                "tanggal_lahir": null,
                "asal_sekolah": null,
                "line_id": null,
                "linkedin": null,
                "instagram_id": null,
                "github_id": null,
                "other_website": null,
                "is_private": false,
                "created_at": "2019-09-18T11:56:53.101630+07:00",
                "updated_at": "2019-09-18T11:56:53.101630+07:00"
            },
            "title": null
        },
        "kata_elemen": "Istri bisa 4 kok", // Disclaimer ini Ari yang bikin
        "created_at": "2019-10-10T14:04:41.233292+07:00",
        "updated_at": "2019-10-10T14:04:41.233292+07:00"
    },
    {
        "id": 4,
        "user": {
            "id": 1,
            "profil": {
                "id": 1,
                "user": {
                    "id": 1,
                    "username": "ari.angga"
                },
                "angkatan": {
                    "id": 1,
                    "tahun": "2018",
                    "nama": "Quanta"
                },
                "interests": [
                    {
                        "interest_name": "Dota",
                        "interest_category": "Technology",
                        "is_top_interest": false
                    }
                ],
                "nama_lengkap": "Ari Angga Nugraha",
                "npm": "1806205086",
                "about": null,
                "jurusan": null,
                "foto": null,
                "email": "ari.angga@ui.ac.id",
                "no_telepon": null,
                "tempat_lahir": null,
                "tanggal_lahir": null,
                "asal_sekolah": null,
                "line_id": null,
                "linkedin": null,
                "instagram_id": null,
                "github_id": null,
                "other_website": null,
                "is_private": false,
                "created_at": "2019-09-18T11:56:53.101630+07:00",
                "updated_at": "2019-09-18T11:56:53.101630+07:00"
            },
            "title": null
        },
        "kata_elemen": "Maun cewek nya ada yang mau sama aku ga ya", // Disclaimer ini Ari yang bikin
        "created_at": "2019-10-10T13:23:17.119558+07:00",
        "updated_at": "2019-10-10T13:23:17.119558+07:00"
    }
  ]
  ```
- Membuat kata elemen baru
  > POST /situs/kata-elemens

  Request body:
  ```json
  {
    "kata_elemen": "Semangat ya Maung!"
  }
  ```

  Hasil sama seperti GET daftar kata elemen, dengan tambahan kata elemen yang baru saja dibuat.

- Mendapatkan detail sebuah kata elemen
  > GET /situs/kata-elemens/{id-kata-elemen}

  e.g: `GET /situs/kata-elemens/1`

  Maka akan mendapatkan objek kata elemen yang pertama (lihat json diatas)

- Menghapus sebuah kata elemen
  > DELETE /situs/kata-elemens/{id-kata-elemen}

  Delete hanya bisa dilakukan oleh user yang membuat kata elemen. Tidak ada response body maupun request body. 

#### Random Corner
```
君の「大丈夫」になりたい 「大丈夫」になりたい
君を大丈夫にしたいんじゃない 君にとっての 「大丈夫」になりたい
```
```
바람을 따라
내 맘도 따라
소나기가 내리는 나의 오후
```
```
Het maakt niet uit hoe langzaam je gaat, zolang je maar niet stopt.
```