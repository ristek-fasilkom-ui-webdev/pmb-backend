from django.contrib.admin.apps import AdminConfig

class PMBBackendAdminConfig(AdminConfig):
    default_site = 'hephaestus.admin.PMBBackendAdminSite'
